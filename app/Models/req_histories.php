<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class req_histories extends Model
{
    protected $guarded = [];
    // protected $fillable = [
    //     'id',
    //     'id_dosen',
    //     'judul',
    //     'penulis',
    //     'penerbit',
    //     'jenis_penerbit',
    //     'tahun_terbit',
    //     'halaman',
    //     'jenis_terbitan',
    //     'link_terbit',
    //     'semester',
    //     'tahun_ajaran',
    //     'sks',
    //     'nama_kegiatan',
    //     'tahun',
    //     'nomor_sertif',
    //     'foto',
    //     'jenis_pengabdian',
    //     'poin',
    //     'alasan',
    //     'type',
    // ];

    public function biodata()
    {
        return $this->belongsTo(biodata::class, 'biodata_id', 'id');
    }

    public function addReq($data){
        return DB::table('req_histories')->insert($data);
    }

    public function info($id){
        return DB::table('req_histories')->where('id', $id)->first();
    }

    public function updateReq($id,$data){
        DB::table('req_histories')->where('id', $id)->update($data);
    }
}