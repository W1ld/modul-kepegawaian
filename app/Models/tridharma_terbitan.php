<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class tridharma_terbitan extends Model
{
    protected $guarded = [];

    public function biodata()
    {
        return $this->belongsTo(biodata::class, 'biodata_id', 'id');
    }

    public function info($id) {
        return DB::table('tridharma_terbitans')->where('biodata_id', $id)->orderBy('waktu_terbit', 'desc')->get();
    }

    public function add($data){
        return DB::table('tridharma_terbitans')->insert($data);
    }

}