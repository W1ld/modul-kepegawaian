<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class tridharma_sks extends Model
{
    protected $guarded = [];

    public function biodata()
    {
        return $this->belongsTo(biodata::class, 'biodata_id', 'id');
    }

    public function info($id) {
        return DB::table('tridharma_sks')->where('biodata_id', $id)
        ->orderBy('tahun_ajaran', 'DESC')
        ->orderBy('semester', 'ASC')
        ->get();
        // SELECT * FROM `tridharma_sks` ORDER BY `tahun_ajaran` DESC, `semester` ASC
    }

    public function add($data){
        return DB::table('tridharma_sks')->insert($data);
    }

}