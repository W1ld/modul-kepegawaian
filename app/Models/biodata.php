<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class biodata extends Model
{
    protected $guarded = [];
    // protected $fillable = [
    //     'nama',
    //     'tgl_lahir',
    //     'tmpt_lahir',
    //     'ktp',
    //     'foto_ktp',
    //     'npwp',
    //     'foto_npwp',
    //     'kk',
    //     'foto_kk',
    //     'pernikahan',
    //     'buku_nikah',
    //     'nama_pasangan',
    //     'asuransi',
    //     'domisili',
    //     'praktek',
    //     'email',
    // ];
    // use HasFactory;

    public function riwayat_kepegawaian()
    {
        return $this->hasMany(riwayat_kepegawaian::class, 'biodata_id', 'id');
    }

    public function riwayat_pendidikan()
    {
        return $this->belongsTo(riwayat_pendidikan::class, 'id', 'biodata_id');
    }

    public function tridharma_pengabdian()
    {
        return $this->hasMany(tridharma_pengabdian::class, 'biodata_id', 'id');
    }

    public function tridharma_sks()
    {
        return $this->hasMany(tridharma_sks::class, 'biodata_id', 'id');
    }

    public function tridharma_terbitan()
    {
        return $this->hasMany(tridharma_terbitan::class, 'biodata_id', 'id');
    }

    public function req_history()
    {
        return $this->hasMany(req_histories::class, 'biodata_id', 'id');
    }

    public function info($id) {
        return DB::table('biodatas')->where('id', $id)->first();
    }

    public function updateBio($id, $data)
    {
        return DB::table('biodatas')->where('id', $id)->update($data);
    }
}