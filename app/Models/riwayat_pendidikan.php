<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class riwayat_pendidikan extends Model
{
    protected $guarded = [];

    public function biodata()
    {
        return $this->hasOne(biodata::class, 'id', 'biodata_id');
    }

}