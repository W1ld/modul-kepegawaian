<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class riwayat_kepegawaian extends Model
{
    protected $guarded = [];

    public function biodata()
    {
        return $this->belongsTo(biodata::class, 'biodata_id', 'id');
    }

}