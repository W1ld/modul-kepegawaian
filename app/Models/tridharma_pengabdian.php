<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class tridharma_pengabdian extends Model
{
    protected $guarded = [];

    public function biodata()
    {
        return $this->belongsTo(biodata::class, 'biodata_id', 'id');
    }

    public function info($id) {
        return DB::table('tridharma_pengabdians')->where('biodata_id', $id)->orderBy('tahun', 'desc')->get();
    }

    public function add($data){
        return DB::table('tridharma_pengabdians')->insert($data);
    }

}