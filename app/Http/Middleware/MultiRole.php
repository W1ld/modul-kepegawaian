<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class MultiRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, String $role)
    {
        if ($role == 'admin_dekan' && in_array(auth()->user()->role,['admin','dekan']))
        return $next($request);

        if ($role == 'dosen_dekan' && in_array(auth()->user()->role,['dosen','dekan']))
        return $next($request);
        elseif (auth()->user()->role == 'admin')
        return redirect('/');

        return redirect('/profil');
    }
}
