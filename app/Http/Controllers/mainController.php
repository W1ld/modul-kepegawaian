<?php

namespace App\Http\Controllers;

use App\Models\biodata;
use App\Models\riwayat_kepegawaian;
use App\Models\riwayat_pendidikan;
use App\Models\tridharma_pengabdian;
use App\Models\tridharma_sks;
use App\Models\tridharma_terbitan;
use App\Models\req_histories;
use App\Models\cpns;
use App\Models\keluarga;
use App\Models\User;
use Illuminate\Http\Request;
use Notification;
use App\Notifications\verifikasiNotification;

class mainController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->biodata = new biodata();
        $this->riwayat_kepegawaian = new riwayat_kepegawaian();
        $this->riwayat_pendidikan = new riwayat_pendidikan();
        $this->tridharma_pengabdian = new tridharma_pengabdian();
        $this->tridharma_sks = new tridharma_sks();
        $this->tridharma_terbitan = new tridharma_terbitan();
        $this->req_histories = new req_histories();
    }

    public function dashboard()
    {
        return view('pages.dashboard');
    }

    public function dashboardz()
    {
        return view('pages.dashboardz');
    }

    public function dosen()
    {
        // $dosen = $this->biodata->rightJoin('riwayat_kepegawaian', 'biodata.id', '=', 'riwayat_kepegawaian.biodata_id')->select('foto', 'nama', 'kum', 'nip_users', 'email', 'riwayat_kepegawaian.no_induk')->get();
        $dosen = biodata::all();
        return view('pages.dosen', compact('dosen'));
    }

    public function detailDosen($nip)
    {
        $dosen = biodata::all();
        $user = $dosen->where('nip_users', $nip)->first();
        $detailUser = $this->detailUser($user->id);
        $pangkat_gol = $detailUser[0];
        $jabatan = $detailUser[1];

        if (Auth()->user()->role == 'admin') {
            $cpns = cpns::where('nip_users', $nip)->get();
            $keluarga = keluarga::where('nip_users', $nip)->get();
            return view('pages.dosen', compact('dosen', 'user', 'pangkat_gol', 'jabatan', 'cpns', 'keluarga'));
        }

        return view('pages.dosen', compact('dosen', 'user', 'pangkat_gol', 'jabatan'));
    }

    public function profil()
    {
        $user = biodata::where('id', auth()->user()->id)->with('riwayat_kepegawaian','riwayat_pendidikan','tridharma_sks','tridharma_pengabdian','tridharma_terbitan')->first();
        $detailUser = $this->detailUser(auth()->user()->id);
        $pangkat_gol = $detailUser[0];
        $jabatan = $detailUser[1];
        return view('pages.my-profile', compact('user', 'pangkat_gol', 'jabatan'));
    }

    private function detailUser($id)
    {
        $detailUser[0] = riwayat_kepegawaian::where('biodata_id', $id)->where('tunjangan', '=', null)->orderBy('tmt', 'desc')->first();
        $detailUser[1] = riwayat_kepegawaian::where('biodata_id', $id)->where('gaji', '=', null)->orderBy('tmt', 'desc')->first();
        return $detailUser;
    }

    public function editBiodata()
    {
        // $buku_nikah = Request()->file('buku_nikah')->store('buku_nikah');
        Request()->validate([
            'pernikahan' => 'required',
            'nama_pasangan' => 'required',
            'asuransi' => 'required',
            'email' => 'required',
            'domisili' => 'required',
            'praktek' => 'required',
        ]);
        $data = [
            'pernikahan' => Request()->pernikahan,
            'nama_pasangan' => Request()->nama_pasangan,
            'asuransi' => Request()->asuransi,
            'email' => Request()->email,
            'domisili' => Request()->domisili,
            'praktek' => Request()->praktek,
            'buku_nikah' => Request()->buku_nikah,
        ];
        $this->biodata->updateBio(auth()->user()->id, $data);
        return redirect()->route('profil');
    }

    public function tridharma()
    {
        $sks = $this->tridharma_sks->info(auth()->user()->id);
        $terbitan = $this->tridharma_terbitan->info(auth()->user()->id);
        $pengabdian = $this->tridharma_pengabdian->info(auth()->user()->id);
        return view('pages.tridharma', compact('sks', 'terbitan', 'pengabdian'));
    }

    // REQUEST FOR VALIDATING
    public function tridharmaSKS(Request $request)
    {
        $request->validate([
            'sks' => 'required',
            'semester' => 'required',
            'tahun_ajaran' => 'required',
        ]);
        $data = [
            'sks' => $request->sks,
            'semester' => $request->semester,
            'tahun_ajaran' => $request->tahun_ajaran,
            'type' => 3,
            'biodata_id' => auth()->user()->id,
        ];
        $this->req_histories->addReq($data);
        $nama = biodata::where('id', auth()->user()->id)->first();
        $thisRequest = req_histories::where('biodata_id', auth()->user()->id)->orderBy('created_at', 'desc')->first();
        $this->makeNotification(4, "$nama->nama telah membuat permintaan verifikasi data SKS", $thisRequest->id);
        return redirect()->route('tridharma')->with('pesan', "Data SKS telah berhasil disimpan, sedang menunggu persetujuan dari akademik");
    }

    public function tridharmaTerbitan(Request $request)
    {
        $request->validate([
            'judul' => 'required',
            'penerbit' => 'required',
            'halaman' => 'required',
            'penulis' => 'required',
            'jenis_penerbitan' => 'required',
            'waktu_terbit' => 'required',
            'jenis_terbitan' => 'required',
            'link_terbit' => 'required',
        ]);
        if ($request->jenis_terbitan == 'jurnal')$tipe = 6;
        elseif ($request->jenis_terbitan == 'buku')$tipe = 9;
        elseif ($request->jenis_terbitan == 'prosiding')$tipe = 12;
        $data = [
            'judul' => $request->judul,
            'penerbit' => $request->penerbit,
            'halaman' => $request->halaman,
            'penulis' => $request->penulis,
            'jenis_penerbitan' => $request->jenis_penerbitan,
            'waktu_terbit' => $request->waktu_terbit,
            'jenis_terbitan' => $request->jenis_terbitan,
            'link_terbit' => $request->link_terbit,
            'type' => $tipe,
            'biodata_id' => auth()->user()->id,
        ];
        $this->req_histories->addReq($data);
        $nama = biodata::where('id', auth()->user()->id)->first();
        $thisRequest = req_histories::where('biodata_id', auth()->user()->id)->where('judul', $request->judul)->first();
        $this->makeNotification(4, "$nama->nama telah membuat permintaan verifikasi data $request->jenis_terbitan", $thisRequest->id);
        return redirect()->route('tridharma')->with('pesan', "Data $request->jenis_terbitan telah berhasil disimpan, sedang menunggu persetujuan dari akademik");
    }

    public function tridharmaPengabdian(Request $request)
    {
        $request->validate([
            'nama_kegiatan' => 'required',
            'tahun' => 'required',
            'jenis_pengabdian' => 'required',
            // 'nomor_sertif' => 'required',
            'foto' => 'required',
        ]);
        $foto = $request->file('foto')->store('assets/tridharma');
        if ($request->jenis_pengabdian == 'seminar')$tipe = 15;
        elseif ($request->jenis_pengabdian == 'lokakarya')$tipe = 18;
        elseif ($request->jenis_pengabdian == 'paten')$tipe = 21;
        elseif ($request->jenis_pengabdian == 'hki')$tipe = 24;
        $data = [
            'nama_kegiatan' => $request->nama_kegiatan,
            'tahun' => $request->tahun,
            'jenis_pengabdian' => $request->jenis_pengabdian,
            'nomor_sertif' => $request->nomor_sertif??null,
            'foto' => $foto,
            'type' => $tipe,
            'biodata_id' => auth()->user()->id,
        ];
        $this->req_histories->addReq($data);
        $nama = biodata::where('id', auth()->user()->id)->first();
        $thisRequest = req_histories::where('biodata_id', auth()->user()->id)->where('nama_kegiatan', $request->nama_kegiatan)->first();
        $this->makeNotification(4, "$nama->nama telah membuat permintaan verifikasi data $request->jenis_pengabdian", $thisRequest->id);
        return redirect()->route('tridharma')->with('pesan', "Data $request->jenis_pengabdian telah berhasil disimpan, sedang menunggu persetujuan dari akademik");
    }

    // REQUEST / VERIFY
    public function request()
    {
        $req_histories = req_histories::with('biodata')->orderBy('id', 'desc')->get();
        $req_histories_done = req_histories::with('biodata')->orderBy('updated_at', 'desc')->get();
        return view('pages.request', compact('req_histories', 'req_histories_done'));
    }

    public function detailRequest($id)
    {
        $req_histories = req_histories::with('biodata')->orderBy('id', 'desc')->get();
        $req_histories_done = req_histories::with('biodata')->orderBy('updated_at', 'desc')->get();
        $req_detail[0] = $this->req_histories->info($id);
        $req_detail[1] = $this->biodata->info($req_detail[0]->biodata_id);
        return view('pages.request', compact('req_histories','req_histories_done','req_detail'));
    }

    public function viewFile($assets,$tridharma,$filename){
        $uri = '/media/'.$assets.'/'.$tridharma.'/'.$filename;
        return redirect()->to($uri);
    }

    public function viewFpd($assets,$tridharma,$filename){
        $uri = 'media/'.$assets.'/'.$tridharma.'/'.$filename;
        // return redirect()->to($uri);
        $pdf = PDF::loadView('pages.request', compact($assets));
        return $pdf->stream($uri);
    }

    public function wRequest(Request $request, $id)
    {
        $get[0] = $this->req_histories->info($id);
        if (isset($request->poin) && !isset($request->alasan)) {
            $get[1] = $this->biodata->info($get[0]->biodata_id);
            $data[0] = [
                'type' => $get[0]->type - 2,
                'poin' => $request->poin,
            ];
            $data[1] = [
                'kum' => $get[1]->kum + $request->poin,
            ];

            if (isset($get[0]->sks)) {
                $data[2] = [
                    'biodata_id' => $get[0]->biodata_id,
                    'semester' => $get[0]->semester,
                    'tahun_ajaran' => $get[0]->tahun_ajaran,
                    'sks' => $get[0]->sks,
                    'poin' => $request->poin,
                ];
                $this->tridharma_sks->add($data[2]);
            } elseif (isset($get[0]->jenis_terbitan)) {
                $data[2] = [
                    'judul' => $get[0]->judul,
                    'penerbit' => $get[0]->penerbit,
                    'halaman' => $get[0]->halaman,
                    'penulis' => $get[0]->penulis,
                    'jenis_penerbitan' => $get[0]->jenis_penerbitan,
                    'waktu_terbit' => $get[0]->waktu_terbit,
                    'jenis_terbitan' => $get[0]->jenis_terbitan,
                    'link_terbit' => $get[0]->link_terbit,
                    'biodata_id' => $get[0]->biodata_id,
                    'poin' => $request->poin,
                ];
                $this->tridharma_terbitan->add($data[2]);
            } elseif (isset($get[0]->jenis_pengabdian)) {
                $data[2] = [
                    'nama_kegiatan' => $get[0]->nama_kegiatan,
                    'tahun' => $get[0]->tahun,
                    'jenis_pengabdian' => $get[0]->jenis_pengabdian,
                    'nomor_sertif' => $get[0]->nomor_sertif,
                    'foto' => $get[0]->foto,
                    'biodata_id' => $get[0]->biodata_id,
                    'poin' => $request->poin,
                ];
                $this->tridharma_pengabdian->add($data[2]);
            }
            $this->biodata->updateBio($get[0]->biodata_id,$data[1]);
            $data[1] = 'Data berhasil disimpan';
            $this->makeNotification($get[0]->biodata_id, "Verifikasi ".(is_null($get[0]->jenis_terbitan)?(is_null($get[0]->jenis_pengabdian)?'SKS':$get[0]->jenis_pengabdian):$get[0]->jenis_terbitan)." Berhasil");
        } elseif (isset($request->alasan) && !isset($request->poin)) {
            $data[0] = ['type' => $get[0]->type - 1,
                        'alasan' => $request->alasan];
            $data[1] = 'Data berhasil ditolak';
            $this->makeNotification($get[0]->biodata_id, "Verifikasi ditolak dengan alasan '$request->alasan'");
        } else
            return redirect()->route('request')->with('pesanEror', 'Eror! silahkan coba kembali');
        $this->req_histories->updateReq($id,$data[0]);
        return redirect()->route('request')->with('pesan', $data[1]);
    }

    // MAKE NOTIF
    private function makeNotification($penerima, $isi, $id=null)
    {
        $user = User::find($penerima);
        $data['pesan'] = $isi;
        if ($user->role == 'admin') {
            $data['link'] = "/verifikasi/$id";
        } elseif ($user->role == 'dekan' || $user->role == 'dosen') {
            $data['link'] = '/tridharma';
        }
        Notification::send($user, new verifikasiNotification($data));
    }

    private function markRead($id=null)
    {
        // GAE HAPUS
        // if ($id != null) {
        //     Auth()->user()->notifications()->where('id', $id)->delete();
        // }
        Auth()->user()->unreadNotifications->markAsRead();
        return back();
    }
}