<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class loginController extends Controller
{
    public function index()
    {
        return view('pages.login');
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->validate([
            'nip' => 'required',
            'password' => 'required'
        ]);
        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();
            if (Auth::user()->role == 'dekan' || Auth::user()->role == 'dosen')
            return redirect()->intended('/tridharma');
            elseif (Auth::user()->role == 'tendik')
            return redirect()->intended('/profil');
            return redirect()->intended('/');
        }
        return back();
    }

    public function register(){
        return view('pages.register');
    }

    public function store(Request $request){
        $validatedData = $request->validate([
            'name' => 'required',
            'nip' => 'required',
            'email' => 'required|email:dns',
            'password' => 'required|min:5'
        ]);
        $validatedData['password'] = Hash::make($validatedData['password']);
        User::create($validatedData);
        return redirect('login');
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('login');
    }
}