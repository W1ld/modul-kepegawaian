<!doctype html>
<html lang="id" dir="ltr">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="icon" href="/favicon.ico" type="image/x-icon"/>
<title>Application | @yield('title')</title>

<!-- Bootstrap Core and vandor -->
<link rel="stylesheet" href="../assets/plugins/bootstrap/css/bootstrap.min.css" />

<!-- Core css -->
<link rel="stylesheet" href="../assets/css/style.min.css"/>

@yield('scriptHead')

</head>

<body class="font-muli theme-cyan">

<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
    </div>
</div>

<div id="main_content">
    <!-- Start Main top header -->
    <div id="header_top" class="header_top">
        <div class="container">
            <div class="hleft">
                <img class="header-brand" src="/favicon.ico" alt="">
                <div class="dropdown">
                    <a href="javascript:void(0)" class="nav-link icon menu_toggle"><i class="fe fe-align-center"></i></a>
                </div>
            </div>
        </div>
    </div>
    <!-- Start Main leftbar navigation -->
    <div id="left-sidebar" class="sidebar">
        <h5 class="brand-name">Kepegawaian<a href="javascript:void(0)" class="menu_option float-right"><i class="icon-grid font-16" data-toggle="tooltip" data-placement="left" title="Grid & List Toggle"></i></a></h5>
        <ul class="nav nav-tabs">
            <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#menu-uni">Menu</a></li>
        </ul>
        <div class="tab-content mt-3">
            <div class="tab-pane fade show active" id="menu-uni" role="tabpanel">
                <nav class="sidebar-nav">
                    <ul class="metismenu">
                        @if (auth()->user()->role == 'admin')
                        <li class="{{request()->is('/')?'active':''}}"><a href="/"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>
                        <li class="{{isset($dosen)?'active':''}}"><a href="/dosen"><i class="fa fa-users"></i><span>Dosen</span></a></li>
                        <li class="{{isset($req_histories)?'active':''}}"><a href="/verifikasi"><i class="fa fa-list-ul"></i><span>Verifikasi</span></a></li>
                        @elseif (Auth::user()->role == 'dekan')
                        <li class="{{request()->is('/')?'active':''}}"><a href="/"><i class="fa fa-dashboard"></i><span>Dashboard</span></a></li>
                        <li class="{{isset($dosen)?'active':''}}"><a href="/dosen"><i class="fa fa-users"></i><span>Dosen</span></a></li>
                        <li class="{{request()->is('tridharma')?'active':''}}"><a href="/tridharma"><i class="fa fa-book"></i><span>Tri Dharma</span></a></li>
                        @elseif (Auth::user()->role == 'dosen')
                        <li class="{{request()->is('tridharma')?'active':''}}"><a href="/tridharma"><i class="fa fa-book"></i><span>Tri Dharma</span></a></li>
                        @elseif (Auth::user()->role == 'tendik')
                        <li class="{{request()->is('profil')?'active':''}}"><a href="/profil"><i class="fa fa-user"></i><span>Profil</span></a></li>
                        @endif
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- Start project content area -->
    <div class="page">
        <!-- Start Page header -->
        <div class="section-body sticky-top" id="page_top" >
            <div class="container-fluid">
                <div class="page-header">
                    <div class="left">
                    </div>
                    <div class="right">
                        <div class="notification d-flex">
                            <div class="dropdown d-flex">
                                <a class="nav-link icon d-none d-md-flex btn btn-default btn-icon ml-1" data-toggle="dropdown" onclick="readNotification()"><i class="fa fa-bell"></i>
                                    @foreach (Auth::user()->notifications as $notification)
                                    @if ($notification->read_at == null)
                                    <span class="badge badge-primary nav-unread" id="unread-notification"></span>
                                    @break
                                    @endif
                                    @endforeach
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <ul class="list-unstyled feeds_widget">
                                        <?php $gaeloop = 0?>
                                        @foreach (Auth::user()->notifications as $notification)
                                        <li>
                                        @if ($notification->read_at == null)
                                        <a href="{{$notification->data['link']}}" id="gaeloop{{$gaeloop++}}" class="notifycation" style="width: 100%;height:100%">{{$notification->data['pesan']}}</a>
                                        @else
                                        <a href="{{$notification->data['link']}}" style="width: 100%;height:100%">{{$notification->data['pesan']}}</a>
                                        @endif
                                        </li>
                                        @endforeach
                                        <?php Auth::user()->unreadNotifications->markAsRead();?>
                                    </ul>
                                    {{-- <hr class="m-0">
                                        <a href="#" name="markAsRead" class="dropdown-item text-center text-muted-dark readall mt-2">Mark all as read</a> --}}
                                </div>
                            </div>
                            <div class="dropdown d-flex">
                                <a href="javascript:void(0)" class="chip ml-3" data-toggle="dropdown">
                                    @if (auth()->user()->role != 'admin')
                                    <span class="avatar" style="background-image: url({{Auth::user()->foto}})"></span> {{Auth::user()->name}}
                                    @else
                                    <span class="avatar avatar-pink" data-toggle="tooltip" data-placement="top" title="Admin">A</span> Admin
                                    @endif
                                </a>
                                <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    @if (auth()->user()->role != 'admin')
                                    <a class="dropdown-item" href="/profil"><i class="dropdown-icon fe fe-user"></i>Profile</a>
                                    @endif
                                    {{-- <a class="dropdown-item" href="#"><i class="dropdown-icon fe fe-settings"></i>Settings</a>
                                    <a class="dropdown-item" href="#"><span class="float-right"><span class="badge badge-primary">6</span></span><i class="dropdown-icon fe fe-mail"></i>Inbox</a>
                                    <a class="dropdown-item" href="#"><i class="dropdown-icon fe fe-send"></i>Message</a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="#"><i class="dropdown-icon fe fe-help-circle"></i>Need help?</a> --}}
                                    <form action="{{ route('logout') }}" method="POST">
                                        @csrf
                                        <button class="dropdown-item" type="submit" style="width:-webkit-fill-available"><i class="dropdown-icon fe fe-log-out"></i>Log out</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Start Page title and tab wwww -->
        @yield('content')
        <!-- Start main footer -->
        <div class="section-body">
            <footer class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            Copyright © 2019 <a href="https://themeforest.net/user/puffintheme/portfolio">PuffinTheme</a>.
                        </div>
                        <div class="col-md-6 col-sm-12 text-md-right">
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item"><a href="../doc/index.html">Documentation</a></li>
                                <li class="list-inline-item"><a href="javascript:void(0)">FAQ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>    
</div>

<!-- Start Main project js, Bootstrap -->
<script src="../assets/bundles/lib.vendor.bundle.js"></script>
<script src="../assets/js/core.js"></script>

@yield('scriptBody')
<script>
</script>
</body>
</html>
