@extends('layout.navbar')
@section('title','Tri Dharma')
@section('scriptHead')
<link rel="stylesheet" href="../assets/plugins/dropify/css/dropify.min.css">
@endsection
@section('scriptBody')
<script src="../assets/js/form/dropify.js"></script>
<script src="../assets/plugins/dropify/js/dropify.min.js"></script>
@endsection
@section('content')
        <div class="section-body">
            <div class="container-fluid">
                <div class="d-flex justify-content-between align-items-center ">
                    <div class="header-action">
                        <h1 class="page-title">RIWAYAT TRI DHARMA</h1>
                    </div>
                    <ul class="nav nav-tabs page-header-tab">
                        <li class="nav-item"><a class="nav-link active"data-toggle="tab" href="#tri-view">Riwayat</a></li>
                        <li class="nav-item"><a class="nav-link"data-toggle="tab" href="#tri-add">Tambah</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="section-body mt-4">
            <div class="container-fluid">
                <div class="tab-content">
                    <div class="tab-pane active" id="tri-view">
                        @if (session('pesan'))
                            <div class="alert alert-success alert-dismissible fade show">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <span><i class="icon fa fa-check"></i> {{session('pesan')}}</span>
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">SKS</h3>
                                <div class="card-options ">
                                    <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                    <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                </div>
                            </div>
                            <div class="card-body">
                                <?php $break = 1;foreach ($sks as $checksks) {
                                    $break = null;
                                    break;
                                } if ($break == 1) echo 'Belum ada data.';
                                else {?>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Semester</th>
                                                <th>Tahun Ajaran</th>
                                                <th>Jumlah SKS</th>
                                                <th>Poin</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1;foreach ($sks as $sks) {
                                                    echo "<tr>
                                                        <td>$no</td>
                                                        <td>$sks->semester</td>
                                                        <td>$sks->tahun_ajaran</td>
                                                        <td>$sks->sks</td>
                                                        <td>$sks->poin</td>
                                                        </tr>";
                                                    $no++;
                                            }?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php }?>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">PENELITIAN DAN PENGEMBANGAN</h3>
                                <div class="card-options ">
                                    <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                    <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                </div>
                            </div>
                            <div class="card-body">
                                <h6>A. JURNAL</h6>
                                <?php $break = 1;foreach ($terbitan as $jurnal) {
                                    if ($jurnal->jenis_terbitan == 'jurnal') {
                                        $break = null;
                                        break;
                                    }
                                }
                                    if ($break == 1) echo 'Belum ada data.<br><br>';
                                    else {
                                ?>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Judul</th>
                                                <th>Jumlah Halaman</th>
                                                <th>Penerbit</th>
                                                <th>Waktu Terbit</th>
                                                <th>Link Jurnal</th>
                                                <th>Poin</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1;foreach ($terbitan as $jurnal) {
                                                // if($jurnal == 'waktu_terbit')$jurnal->waktu_terbit = date_create($waktu_terbit);
                                                if ($jurnal->jenis_terbitan == 'jurnal') {
                                                    echo "<tr>
                                                        <td>$no</td>
                                                        <td>$jurnal->judul</td>
                                                        <td>$jurnal->halaman</td>
                                                        <td>$jurnal->penerbit</td>
                                                        <td>$jurnal->waktu_terbit</td>";
                                                    echo isset($jurnal->link_terbit)?"<td><a href='$jurnal->link_terbit' target='_blank'>$jurnal->penerbit</a></td>":"<td></td>";
                                                    echo "<td>$jurnal->poin</td></tr>";
                                                    $no++;
                                                }
                                            }?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php }?>
                                <h6>B. BUKU</h6>
                                <?php $break = 1;foreach ($terbitan as $buku) {
                                    if ($buku->jenis_terbitan == 'buku') {
                                        $break = null;
                                        break;
                                    }
                                }
                                    if ($break == 1) echo 'Belum ada data.<br>';
                                    else {
                                ?>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Judul</th>
                                                <th>Jumlah Halaman</th>
                                                <th>Penerbit</th>
                                                <th>Tahun Terbit</th>
                                                <th>Poin</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1;foreach ($terbitan as $buku) {
                                                if ($buku->jenis_terbitan == 'buku') {
                                                    echo "<tr>
                                                        <td>$no</td>
                                                        <td>$buku->judul</td>
                                                        <td>$buku->halaman</td>
                                                        <td>$buku->penerbit</td>
                                                        <td>$buku->waktu_terbit</td>
                                                        <td>$buku->poin</td>
                                                        </tr>";
                                                    $no++;
                                                }
                                            }?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php }?>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">PENGABDIAN MASYARAKAT</h3>
                                <div class="card-options ">
                                    <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                    <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                </div>
                            </div>
                            <div class="card-body">
                                <h6>A. SEMINAR</h6>
                                <?php $break = 1;foreach ($pengabdian as $seminar) {
                                    if ($seminar->jenis_pengabdian == 'seminar') {
                                        $break = null;
                                        break;
                                    }
                                }
                                    if ($break == 1) echo 'Belum ada data.<br><br>';
                                    else {
                                ?>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Kegiatan</th>
                                                <th>Tahun Pelaksanaan</th>
                                                <th>Poin</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1;foreach ($pengabdian as $seminar) {
                                                if ($seminar->jenis_pengabdian == 'seminar') {
                                                    echo "<tr>
                                                        <td>$no</td>
                                                        <td>$seminar->nama_kegiatan</td>
                                                        <td>$seminar->tahun</td>
                                                        <td>$seminar->poin</td>
                                                        </tr>";
                                                    $no++;
                                                }
                                            }?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php }?>
                                <h6>B. LOKAKARYA</h6>
                                <?php $break = 1;foreach ($pengabdian as $lokakarya) {
                                    if ($lokakarya->jenis_pengabdian == 'lokakarya') {
                                        $break = null;
                                        break;
                                    }
                                }
                                    if ($break == 1) echo 'Belum ada data.<br><br>';
                                    else {
                                ?>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Kegiatan</th>
                                                <th>Tahun Pelaksanaan</th>
                                                <th>Poin</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1;foreach ($pengabdian as $lokakarya) {
                                                if ($lokakarya->jenis_pengabdian == 'lokakarya') {
                                                    echo "<tr>
                                                        <td>$no</td>
                                                        <td>$lokakarya->nama_kegiatan</td>
                                                        <td>$lokakarya->tahun</td>
                                                        <td>$lokakarya->poin</td>
                                                        </tr>";
                                                    $no++;
                                                }
                                            }?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php }?>
                                <h6>C PROSIDING</h6>
                                <?php $break = 1;foreach ($terbitan as $proceeding) {
                                    if ($proceeding->jenis_terbitan == 'prosiding') {
                                        $break = null;
                                        break;
                                    }
                                }
                                    if ($break == 1) echo 'Belum ada data.<br><br>';
                                    else {
                                ?>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Judul</th>
                                                <th>Jumlah Halaman</th>
                                                <th>Penerbit</th>
                                                <th>Tahun Terbit</th>
                                                <th>Poin</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1;foreach ($terbitan as $proceeding) {
                                                if ($proceeding->jenis_terbitan == 'prosiding') {
                                                    echo "<tr>
                                                        <td>$no</td>
                                                        <td>$proceeding->judul</td>
                                                        <td>$proceeding->halaman</td>
                                                        <td>$proceeding->penerbit</td>
                                                        <td>$proceeding->waktu_terbit</td>
                                                        <td>$proceeding->poin</td>
                                                        </tr>";
                                                    $no++;
                                                }
                                            }?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php }?>
                                <h6>D. HAK PATEN</h6>
                                <?php $break = 1;foreach ($pengabdian as $paten) {
                                    if ($paten->jenis_pengabdian == 'paten') {
                                        $break = null;
                                        break;
                                    }
                                }
                                    if ($break == 1) echo 'Belum ada data.<br><br>';
                                    else {
                                ?>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Produk</th>
                                                <th>Tahun</th>
                                                <th>Nomor Sertifikat</th>
                                                <th>Poin</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1;foreach ($pengabdian as $paten) {
                                                if ($paten->jenis_pengabdian == 'paten') {
                                                    echo "<tr>
                                                        <td>$no</td>
                                                        <td>$paten->nama_kegiatan</td>
                                                        <td>$paten->tahun</td>
                                                        <td>$paten->nomor_sertif</td>
                                                        <td>$paten->poin</td>
                                                        </tr>";
                                                    $no++;
                                                }
                                            }?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php }?>
                                <h6>E. SERTIFIKAT HKI</h6>
                                <?php $break = 1;foreach ($pengabdian as $hki) {
                                    if ($hki->jenis_pengabdian == 'hki') {
                                        $break = null;
                                        break;
                                    }
                                }
                                    if ($break == 1) echo 'Belum ada data.<br>';
                                    else {
                                ?>
                                <div class="table-responsive">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Produk</th>
                                                <th>Tahun</th>
                                                <th>Nomor Sertifikat</th>
                                                <th>Poin</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $no = 1;foreach ($pengabdian as $hki) {
                                                if ($hki->jenis_pengabdian == 'hki') {
                                                    echo "<tr>
                                                        <td>$no</td>
                                                        <td>$hki->nama_kegiatan</td>
                                                        <td>$hki->tahun</td>
                                                        <td>$paten->nomor_sertif</td>
                                                        <td>$hki->poin</td>
                                                        </tr>";
                                                    $no++;
                                                }
                                            }?>
                                        </tbody>
                                    </table>
                                </div>
                                <?php }?>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tri-add">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">SKS</h3>
                                <div class="card-options ">
                                    <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                    <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                </div>
                            </div>
                            <form action="/tridharma/sks" method="post">@csrf
                                <div class="card-body">
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Jumlah SKS</label>
                                                <input type="number" name="sks" class="form-control" required min=2 max=123>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Semester</label>
                                                <select class="form-control" name="semester" required>
                                                    <option></option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Tahun Ajaran</label>
                                                <select class="form-control show-tick" name="tahun_ajaran" required>
                                                    <option></option>
                                                    <option value="2023/2024">2023/2024</option>
                                                    <option value="2022/2023">2022/2023</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="/tridharma"><button type="button" class="btn btn-outline-secondary">Cancel</button></a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">PENELITIAN DAN PENGEMBANGAN</h3>
                                <div class="card-options ">
                                    <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                    <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                </div>
                            </div>
                            <div class="card-body">
                                <form action="/tridharma/terbitan" method="post">@csrf
                                    <input type="hidden" name="jenis_terbitan" value="jurnal">
                                    <h6>A. JURNAL</h6>
                                    <div class="row clearfix">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Judul Jurnal</label>
                                                <input type="text" name="judul" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Penulis</label>
                                                <input type="text" name="penulis" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Penerbit</label>
                                                <input type="text" name="penerbit" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Penerbitan</label>
                                                <select class="form-control show-tick" name="jenis_penerbitan" required>
                                                    <option></option>
                                                    <option value="Nasional">Nasional</option>
                                                    <option value="Internasional">Internasional</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>Jumlah Halaman</label>
                                                <input type="text" name="halaman" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>Waktu Terbit</label>
                                                <input type="date" name="waktu_terbit" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Link Penerbitan</label>
                                                <input type="text" name="link_terbit" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="/tridharma"><button type="button" class="btn btn-outline-secondary">Cancel</button></a>
                                        </div>
                                    </div>
                                </form>
                                <form action="/tridharma/terbitan" method="post">@csrf
                                    <input type="hidden" name="jenis_terbitan" value="buku">
                                    <br><br><h6>B. BUKU</h6>
                                    <div class="row clearfix">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Judul Buku</label>
                                                <input type="text" name="judul" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Penulis</label>
                                                <input type="text" name="penulis" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Penerbit</label>
                                                <input type="text" name="penerbit" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Penerbitan</label>
                                                <select class="form-control show-tick" name="jenis_penerbitan" required>
                                                    <option></option>
                                                    <option value="Nasional">Nasional</option>
                                                    <option value="Internasional">Internasional</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>Jumlah Halaman</label>
                                                <input type="text" name="halaman" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>Waktu Terbit</label>
                                                <input type="date" name="waktu_terbit" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Link Penerbitan??</label>
                                                <input type="text" name="link_terbit" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="/tridharma"><button type="button" class="btn btn-outline-secondary">Cancel</button></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">PENGABDIAN MASYARAKAT</h3>
                                <div class="card-options ">
                                    <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                    <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                </div>
                            </div>
                            <div class="card-body">
                                <form action="/tridharma/pengabdian" method="post" enctype="multipart/form-data">@csrf
                                    <input type="hidden" name="jenis_pengabdian" value="seminar">
                                    <h6>A. SEMINAR</h6>
                                    <div class="row clearfix">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Nama Kegiatan</label>
                                                <input type="text" name="nama_kegiatan" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>Tahun</label>
                                                <input type="number" name="tahun" class="form-control" max="2023">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Foto Kegiatan</label>
                                                <div class="form-group mt-2 mb-3">
                                                    <input type="file" id="foto" name="foto" class="dropify" accept=".jpg, .jpeg, .png, .pdf" required>
                                                    <small id="fileHelp" class="form-text text-muted">Format yang didukung jpg, jpeg, png, atau dijadikan satu file pdf</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="/tridharma"><button type="button" class="btn btn-outline-secondary">Cancel</button></a>
                                        </div>
                                    </div>
                                </form>
                                <form action="/tridharma/pengabdian" method="post" enctype="multipart/form-data">@csrf
                                    <input type="hidden" name="jenis_pengabdian" value="lokakarya">
                                    <br><br><h6>B. LOKAKARYA</h6>
                                    <div class="row clearfix">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Nama Kegiatan</label>
                                                <input type="text" name="nama_kegiatan" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>Tahun</label>
                                                <input type="number" name="tahun" class="form-control" max="2023">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Foto Kegiatan</label>
                                                <div class="form-group mt-2 mb-3">
                                                    <input type="file" id="foto" name="foto" class="dropify" accept=".jpg, .jpeg, .png, .pdf" required>
                                                    <small id="fileHelp" class="form-text text-muted">Format yang didukung jpg, jpeg, png, atau dijadikan satu file pdf</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="/tridharma"><button type="button" class="btn btn-outline-secondary">Cancel</button></a>
                                        </div>
                                    </div>
                                </form>
                                <form action="/tridharma/terbitan" method="post">@csrf
                                    <input type="hidden" name="jenis_terbitan" value="prosiding">
                                    <br><br><h6>C. PROSIDING</h6>
                                    <div class="row clearfix">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Judul Prosiding</label>
                                                <input type="text" name="judul" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label>Penulis</label>
                                                <input type="text" name="penulis" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Penerbit</label>
                                                <input type="text" name="penerbit" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Penerbitan</label>
                                                <select class="form-control show-tick" name="jenis_penerbitan" required>
                                                    <option></option>
                                                    <option value="Nasional">Nasional</option>
                                                    <option value="Internasional">Internasional</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>Jumlah Halaman</label>
                                                <input type="text" name="halaman" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>Waktu Terbit</label>
                                                <input type="date" name="waktu_terbit" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label>Link Penerbitan??</label>
                                                <input type="text" name="link_terbit" class="form-control" required>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="/tridharma"><button type="button" class="btn btn-outline-secondary">Cancel</button></a>
                                        </div>
                                    </div>
                                </form>
                                <form action="/tridharma/pengabdian" method="post" enctype="multipart/form-data">@csrf
                                    <input type="hidden" name="jenis_pengabdian" value="paten">
                                    <br><br><h6>D. HAK PATEN</h6>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Nama Hal</label>
                                                <input type="text" name="nama_kegiatan" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>Tahun</label>
                                                <input type="number" name="tahun" class="form-control" max="2023">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>Nomor Sertifikat</label>
                                                <input type="text" name="nomor_sertif" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Sertifikat</label>
                                                <div class="form-group mt-2 mb-3">
                                                    <input type="file" id="foto" name="foto" accept=".jpg, .jpeg, .png, .pdf" required>
                                                    <small id="fileHelp" class="form-text text-muted">Format yang didukung jpg, jpeg, png, pdf</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="/tridharma"><button type="button" class="btn btn-outline-secondary">Cancel</button></a>
                                        </div>
                                    </div>
                                </form>
                                <form action="/tridharma/pengabdian" method="post" enctype="multipart/form-data">@csrf
                                    <input type="hidden" name="jenis_pengabdian" value="hki">
                                    <br><br><h6>E. SERTIFIKAT HKI</h6>
                                    <div class="row clearfix">
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Nama Kekayaan Intelektual</label>
                                                <input type="text" name="nama_kegiatan" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>Tahun</label>
                                                <input type="number" name="tahun" class="form-control" max="2023">
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <div class="form-group">
                                                <label>Nomor Sertifikat</label>
                                                <input type="text" name="nomor_sertif" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-sm-4">
                                            <div class="form-group">
                                                <label>Sertifikat</label>
                                                <div class="form-group mt-2 mb-3">
                                                    <input type="file" id="foto" name="foto" accept=".jpg, .jpeg, .png, .pdf" required>
                                                    <small id="fileHelp" class="form-text text-muted">Format yang didukung jpg, jpeg, png, pdf</small>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                            <a href="/tridharma"><button type="button" class="btn btn-outline-secondary">Cancel</button></a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endsection
