@extends('layout.navbar')
@section('title','Verifikasi')
@section('scriptHead')
{{-- table --}}
<link rel="stylesheet" href="../assets/plugins/datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/plugins/datatable/fixedeader/dataTables.fixedcolumns.bootstrap4.min.css">
<link rel="stylesheet" href="../assets/plugins/datatable/fixedeader/dataTables.fixedheader.bootstrap4.min.css">
<!-- jquery -->
<script src="https://code.jquery.com/jquery-3.6.2.min.js" integrity="sha256-2krYZKh//PcchRtd+H+VyyQoZ/e3EcrkxhM8ycwASPA=" crossorigin="anonymous"></script>
@endsection
@section('scriptBody')
{{-- table --}}
<script src="../assets/bundles/dataTables.bundle.js"></script>
<script src="../assets/js/page/dialogs.js"></script>
<script src="../assets/js/table/datatable.js"></script>
@endsection
@section('content')
        <div class="section-body">
            <div class="container-fluid">
                <div class="d-flex justify-content-between align-items-center ">
                    <div class="header-action">
                        <h1 class="page-title">LIST VERIFIKASI</h1>
                    </div>
                    <ul class="nav nav-tabs page-header-tab">
                    @if (isset($req_detail))
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#req-detail">Detail</a></li>
                    @endif
                        <li class="nav-item"><a class="nav-link {{request()->is('verifikasi')?(null !== session('pesan')?'':'active'):''}}" data-toggle="tab" href="#req-list">Request</a></li>
                        <li class="nav-item"><a class="nav-link {{null !== session('pesan')?'active':''}}" data-toggle="tab" href="#req-done">History</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="section-body mt-4">
            <div class="container-fluid">
                <div class="tab-content">
                    @if (isset($req_detail))
                    <div class="tab-pane active" id="req-detail">
                        <div class="row row-deck">
                            <div class="col-12">
                                <div class="card">
                                    <div class="card-body detail">
                                        <div class="detail-header">
                                            <div class="media">
                                                <div class="float-left">
                                                    <div class="mr-2"><img src="{{$req_detail[1]->foto}}" class="rounded-circle profil-photo-request" alt=""></div>
                                                </div>
                                                <div class="media-body">
                                                    <p class="mb-0">
                                                        <a href="/dosen/{{$req_detail[1]->nip_users}}">{{$req_detail[1]->nama}}</a>
                                                        {{-- <span class="text-muted text-sm float-right">{{date('d F Y',strtotime($req_detail[0]->created_at))}}</span> --}}
                                                        <span class="text-muted text-sm float-right">{{date('d-m-Y',strtotime($req_detail[0]->created_at))}}</span>
                                                    </p>
                                                    <span class="text-muted">
                                                        <?php if ($req_detail[0]->type<=3)echo'Input data SKS';
                                                        elseif ($req_detail[0]->type<=6)echo'Tambah data jurnal';
                                                        elseif ($req_detail[0]->type<=9)echo'Tambah data Buku';
                                                        elseif ($req_detail[0]->type<=12)echo'Tambah data Prosiding';
                                                        elseif ($req_detail[0]->type<=15)echo'Tambah data Seminar';
                                                        elseif ($req_detail[0]->type<=18)echo'Tambah data Lokakarya';
                                                        elseif ($req_detail[0]->type<=21)echo'Tambah data Hak Paten';
                                                        elseif ($req_detail[0]->type<=24)echo'Tambah data Sertifikat HKI';
                                                        ?></span>
                                                    <span class="text-muted text-sm float-right">{{date('H:i',strtotime($req_detail[0]->created_at))}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mail-cnt mt-3">
                                            <div class="row">
                                                <div class="table-responsive col-md-9 col-sm-12">
                                                    <table class="table table-borderless">
                                                        <tbody>
                                                            <?php $name = ['judul', 'penulis', 'penerbit', 'jenis_penerbitan', 'waktu_terbit', 'halaman', 'link_terbit', 'semester', 'tahun_ajaran', 'sks', 'nama_kegiatan', 'tahun', 'nomor_sertif', 'jenis_pengabdian', 'poin'];
                                                            foreach ($name as $name) {
                                                                if (isset($req_detail[0]->$name)) {
                                                                    $replace = str_replace('_', ' ', $name);
                                                                    echo "<tr><th>$replace</th>";
                                                                    if ($name == 'link_terbit') {
                                                                        echo "<td><a href='".$req_detail[0]->link_terbit."' target='_blank'><p style='white-space: nowrap; width: 300px; overflow: hidden; text-overflow: ellipsis;'>".$req_detail[0]->link_terbit."</p></a></td></tr>";
                                                                        continue;
                                                                    }
                                                                    echo "<td>" . $req_detail[0]->$name . "</td></tr>";
                                                                }
                                                            }?>
                                                            <?php $name = ['foto'];
                                                            foreach ($name as $name) {
                                                                if (isset($req_detail[0]->$name)) {
                                                                    $foto = request()->getHttpHost().'/media/assets/'.$req_detail[0]->$name;
                                                            ?>

                                                            <tr>
                                                                <th>File</th>
                                                                <td>
                                                                    <a href="/verifikasi/{{$req_detail[0]->$name}}" target="_blank">
                                                                        <div class="icon">
                                                                            <i class="fa fa-file-o text-info"></i>
                                                                        </div>
                                                                        <div class="file-name">
                                                                            <p class="mb-0 text-muted">View File (<?php

                                                                                $ext = pathinfo($req_detail[0]->$name, PATHINFO_EXTENSION);
                                                                                
                                                                                echo $ext;
                                                                                ?>)</p>
                                                                            <small>{{filesize('media/'.$req_detail[0]->$name)}} bytes</small>
                                                                        </div>
                                                                    </a>
                                                                </td>
                                                            </tr>
                                                            <?php }}?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            @if (isset($req_detail[0]->updated_at))
                                            </div>
                                            <div class="align-center bg-dark p-2 text-white">
                                                @if(in_array($req_detail[0]->type,[2,5,8,11,14,17,20,23]))
                                                <span class="text-danger"><b>Ditolak</b></span>
                                                @else
                                                <span class="text-green">Diterima dengan poin {{$req_detail[0]->poin}}</span>
                                                @endif
                                                pada {{date('H:i d-m-Y',strtotime($req_detail[0]->updated_at))}}
                                            </div>
                                            @else
                                                <div class="table-responsive col-md-3 col-sm-12">
                                                    <table class="table table-hover border">
                                                        <tbody>
                                                            <tr>
                                                                <th>sks</th>
                                                                <td>0.3</td>
                                                            </tr>
                                                            <tr>
                                                                <th>jurnal</th>
                                                                <td>2.75</td>
                                                            </tr>
                                                            <tr>
                                                                <th>buku</th>
                                                                <td>5</td>
                                                            </tr>
                                                            <tr>
                                                                <th>seminar</th>
                                                                <td>2.8</td>
                                                            </tr>
                                                            <tr>
                                                                <th>lokakarya</th>
                                                                <td>2.1</td>
                                                            </tr>
                                                            <tr>
                                                                <th>prosiding</th>
                                                                <td>2.1</td>
                                                            </tr>
                                                            <tr>
                                                                <th>hak paten</th>
                                                                <td>2.25</td>
                                                            </tr>
                                                            <tr>
                                                                <th>sertifikat HKI</th>
                                                                <td>2.09</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-md-3 col-sm-12 align-right">
                                                    <div class="row">
                                                        <div class="col-4">
                                                            <label class="mt-3"><b>Poin: </b> </label>
                                                        </div>
                                                        <div class="col-8">
                                                            <input class="form-control w-75 mt-2 align-center" type="number" name="poin" id="poin" required min="0.01" value="0.00" step="0.01">
                                                            {{-- onchange="(function(el){el.value=parseFloat(el.value).toFixed(2);})(this)" --}}
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12">
                                                    <button id="forModal" type="button" data-id="y" data-toggle="modal" data-target="#acceptModal" class="btn text-white text-uppercase w-100 mt-2 mb-2" style="background:#007bff">terima</button>
                                                </div>
                                                <div class="col-md-3 col-sm-12">
                                                    <button id="forModal" type="button" data-toggle="modal" data-target="#declineModal" class="btn text-white text-uppercase w-100 mt-2 mb-2" style="background:#dc3545">tolak</button>
                                                </div>
                                                <div class="col-md-3 col-sm-12">
                                                    <a href="/verifikasi"><button class="btn text-white text-uppercase w-100 mt-2 mb-2 bg-secondary">batal</button></a>
                                                </div>
                                            </div>
                                        @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                            {{-- MODAL --}}
                    <div class="modal fade" id="acceptModal" tabindex="-1" role="dialog" aria-labelledby="acceptModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="acceptModalLabel">Konfirmasi</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <span id="bodyModal"></span>
                                </div>
                                <div class="modal-footer">
                                    <form action="/verifikasi/w/{{$req_detail[0]->id}}" method="post">@csrf
                                        <input type="hidden" name="poin" id="inputPoin" value="">
                                        <input type="hidden" name="alasan" id="inputAlasan" value="">
                                        <button type="submit" class="btn btn-primary">Ya</button>
                                    </form>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tidak</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade" id="declineModal" tabindex="-1" role="dialog" aria-labelledby="declineModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="declineModalLabel">Konfirmasi</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <span id="bodyModal">
                                        <label for="alasan">Alasan menolak:</label>
                                        <input type="text" name="alasan" id="alasan" class="w-100" value="">
                                    </span>
                                </div>
                                <div class="modal-footer">
                                        <button type="button" id="forModal" data-id="n" data-toggle="modal" data-target="#acceptModal" data-dismiss="modal" class="btn btn-primary">Kirim</button>
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="tab-pane {{isset($req_detail)?'':(null !== session('pesan')?'':'active')}}" id="req-list">
                        <div class="row">
                            <div class="col-12">
                                @if (session('pesanEror'))
                                <div class="alert alert-danger alert-dismissible fade show">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <span><i class="icon fa fa-times"></i> {{session('pesanEror')}}</span>
                                </div>
                                @endif
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <?php $break = 1;foreach ($req_histories as $checkreq) {
                                                if(in_array($checkreq->type,[3,6,9,12,15,18,21,24])){
                                                    $break = null;
                                                    break;
                                                }
                                            } if ($break == 1) echo '<p class="m-5 align-center">Belum ada permintaan verifikasi.</p>';
                                            else {$nomor = 1;?>
                                            <table id='datatable' class="table table-striped table-hover table-sm table_custom js-basic-example dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Nama</th>
                                                        <th>NIP</th>
                                                        <th>Waktu</th>
                                                        <th>Permintaan</th>
                                                        <th class="no-sort">Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($req_histories as $pending)
                                                    @if (in_array($pending->type,[3,6,9,12,15,18,21,24]))
                                                    <tr>
                                                        <td>{{$nomor++}}</td>
                                                        <td><div class="font-15">{{$pending->biodata->nama}}</div></td>
                                                        <td><span>{{$pending->biodata->nip_users}}</span></td>
                                                        <td>{{date('d-m-Y',strtotime($pending->created_at))}}</td>
                                                        <td><?php if ($pending->type==3)echo'Input data SKS';
                                                        elseif ($pending->type==6)echo'Tambah data jurnal';
                                                        elseif ($pending->type==9)echo'Tambah data Buku';
                                                        elseif ($pending->type==12)echo'Tambah data Prosiding';
                                                        elseif ($pending->type==15)echo'Tambah data Seminar';
                                                        elseif ($pending->type==18)echo'Tambah data Lokakarya';
                                                        elseif ($pending->type==21)echo'Tambah data Hak Paten';
                                                        elseif ($pending->type==24)echo'Tambah data Sertifikat HKI'?></td>
                                                        <td>
                                                            <form action="/verifikasi/{{$pending->id}}" method="get">
                                                                <button class="badge badge-primary p-2 border-0" type="submit"><i class="fa fa-info-circle fa-lg"></i>&nbsp Detail</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            <?php }$nomor = 1;?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane {{null !== session('pesan')?'active':''}}" id="req-done">
                        <div class="row">
                            <div class="col-12">
                                @if (session('pesan'))
                                <div class="alert alert-success alert-dismissible fade show">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                    <span><i class="icon fa fa-check"></i> {{session('pesan')}}</span>
                                </div>
                                @endif
                                <div class="card">
                                    <div class="card-body">
                                        <div class="table-responsive">                                
                                            <table id='datatable' class="table table-striped table-hover table-sm table_custom js-basic-example dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>No.</th>
                                                        <th>Nama</th>
                                                        <th>NIP</th>
                                                        <th>Waktu</th>
                                                        <th>Permintaan</th>
                                                        <th>Status</th>
                                                        <th>Poin</th>
                                                        <th class="no-sort">Aksi</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($req_histories_done as $done)
                                                    @if (in_array($done->type,[1,2,4,5,7,8,10,11,13,14,16,17,19,20,22,23]))
                                                    <tr class="align-middle">
                                                        <td>{{$nomor++}}</td>
                                                        <td><div class="font-15">{{$done->biodata->nama}}</div></td>
                                                        <td><span>{{$done->biodata->nip_users}}</span></td>
                                                        <td>{{date('d-m-Y',strtotime($done->updated_at))}}</td>
                                                        <td><?php if ($done->type < 3)echo'Input data SKS';
                                                        elseif ($done->type < 6)echo'Tambah data jurnal';
                                                        elseif ($done->type < 9)echo'Tambah data Buku';
                                                        elseif ($done->type < 12)echo'Tambah data Prosiding';
                                                        elseif ($done->type < 15)echo'Tambah data Seminar';
                                                        elseif ($done->type < 18)echo'Tambah data Lokakarya';
                                                        elseif ($done->type < 21)echo'Tambah data Hak Paten';
                                                        elseif ($done->type < 24)echo'Tambah data Sertifikat HKI'?></td>
                                                        <td><?php if (in_array($done->type,[1,4,7,10,13,16,19,22]))echo'
                                                                <span class="tag tag-green">diterima</span>';
                                                                elseif (in_array($done->type,[2,5,8,11,14,17,20,23]))echo'
                                                                <span class="tag tag-red">ditolak</span>'?>
                                                        </td>
                                                        <td>{{$done->poin}}</td>
                                                        <td>
                                                            <form action="/verifikasi/{{$done->id}}" method="get">
                                                                <button class="badge badge-primary p-2 border-0" type="submit"><i class="fa fa-info-circle fa-lg"></i>&nbsp Detail</button>
                                                            </form>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            // $(document).on("click", "#alasan", function () {
            //     var alasan = $('#alasan').val();
            //     $("#inputAlasan").val( alasan );
            // });
            $(document).on("click", "#forModal", function () {
                var id = $(this).data('id');
                if (id == "y") {
                    var poin = $('#poin').val();
                    $("#inputPoin").val( poin );
                    $("#inputAlasan").val( null );
                    $("#bodyModal").html("Yakin ingin menyimpan dengan poin "+poin+'?');
                }
                else if (id == "n") {
                    var alasan = $('#alasan').val();
                    $("#inputAlasan").val( alasan );
                    $("#inputPoin").val( null );
                    $("#bodyModal").html("Yakin ingin menolak permintaan?");
                }
            });
            $(document).ready(function () {
                $('#datatable').DataTable({
                    columnDefs: [{ orderable: false, targets: 'no-sort'}] //disable sorting
                    // columnDefs: [{ orderable: false, targets: [0,7] }] //another disable sorting
                });
            });
        </script>
        @endsection