@extends('layout.navbar')
@section('title','Dosen')
@section('scriptHead')
<link rel="stylesheet" href="../assets/plugins/dropify/css/dropify.min.css">
@endsection
@section('scriptBody')
<script src="../assets/js/form/dropify.js"></script>
<script src="../assets/plugins/dropify/js/dropify.min.js"></script>
@endsection
@section('content')
<div class="section-body">
            <div class="container-fluid">
                <div class="d-flex justify-content-between align-items-center ">
                    <div class="header-action">
                        <h1 class="page-title">Dosen</h1>
                    </div>
                    <ul class="nav nav-tabs page-header-tab">
                        @if (isset($user))
                        @if (Auth::user()->role == 'admin')
                        <li class="nav-item"><a class="nav-link"data-toggle="tab" href="#pro-edit">Edit</a></li>
                        <li class="nav-item"><a class="nav-link"data-toggle="tab" href="#pro-detail">Profile (Detail)</a></li>
                        @endif
                        <li class="nav-item"><a class="nav-link active"data-toggle="tab" href="#pro-profile">Profile</a></li>
                        @endif
                        <li class="nav-item"><a class="nav-link <?= isset($user)?'':'active';?>"data-toggle="tab" href="#pro-grid">Semua Dosen</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="section-body mt-4">
            <div class="container-fluid">
                <div class="tab-content">
                    <div class="tab-pane <?= isset($user)?'':'active'?>" id="pro-grid">
                        <div class="row">
                            @foreach($dosen as $dosens)
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <img class="card-profile-img" src="{{$dosens->foto}}" alt="">
                                        <h5 class="mb-0 nama-dosen">{{$dosens->nama}}</h5>
                                        <div class="text-muted">{{$dosens->nip_users}}</div>
                                        <div class="mt-3">{{$dosens->prodi}}</div>
                                        <p>{{$dosens->kum}}</p>
                                        <a href="/dosen/{{$dosens->nip_users}}"><button class="btn btn-primary btn-sm">Read More</button></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @foreach($dosen as $dosenn)
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <img class="card-profile-img" src="{{$dosenn->foto}}" alt="">
                                        <h5 class="mb-0 nama-dosen">{{$dosenn->nama}}</h5>
                                        <div class="text-muted">{{$dosenn->nip_users}}</div>
                                        <div class="mt-3">{{$dosenn->prodi}}</div>
                                        <p>{{$dosenn->kum}}</p>
                                        <a href="/dosen/{{$dosenn->nip_users}}"><button class="btn btn-primary btn-sm">Read More</button></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                            @foreach($dosen as $dosen)
                            <div class="col-xl-3 col-lg-4 col-md-6">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <img class="card-profile-img" src="{{$dosen->foto}}" alt="">
                                        <h5 class="mb-0 nama-dosen">{{$dosen->nama}}</h5>
                                        <div class="text-muted">{{$dosen->nip_users}}</div>
                                        <div class="mt-3">{{$dosen->prodi}}</div>
                                        <p>{{$dosen->kum}}</p>
                                        <a href="/dosen/{{$dosen->nip_users}}"><button class="btn btn-primary btn-sm">Detail</button></a>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    @if (isset($user))
                    <div class="tab-pane active" id="pro-profile">
                        <div class="row">
                            <div class="col-xl-4 col-md-12">
                                <div class="card">
                                    <div class="card-body w_user">
                                        <div class="user_avatar">
                                            <img class="rounded-circle profil-photo" style="background:url({{$user->foto}})" src="{{$user->foto}}" alt="">
                                        </div>
                                        <div class="wid-u-info">
                                            <h5 style="max-width:160px;min-height:2em">{{$user->nama}}</h5>
                                            <p class="text-muted mb-0">{{$user->prodi}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Biodata</h3>
                                        <div class="card-options ">
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                            <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                        </div>
                                    </div>
									<div class="card-body">
										<ul class="list-group">
											<li class="list-group-item">
												<b>NIK</b>
												<div class="profile-desc-item pull-right">{{$user->nik}}</div>
											</li>
											<li class="list-group-item">
												<b>Serdos</b>
												<div class="profile-desc-item pull-right">{{$user->serdos}}</div>
											</li>
											<li class="list-group-item">
												<b>NIDN</b>
												<div class="profile-desc-item pull-right">{{$user->nidn}}</div>
											</li>
											<li class="list-group-item">
												<b>Tempat dan</b>
												<div class="profile-desc-item pull-right">{{$user->tmpt_lahir}}</div>
											</li>
											<li class="list-group-item">
												<b>Tanggal Lahir</b>
												<div class="profile-desc-item pull-right">{{date('d-m-Y',strtotime($user->tgl_lahir))}}</div>
											</li>
											{{-- <li class="list-group-item">
												<b>Status</b>
												<div class="profile-desc-item pull-right"><?php if ($user->pernikahan == "sudah") echo "Sudah Menikah"; else echo "Belum Menikah"?></div>
											</li> --}}
											<li class="list-group-item">
												<b>Email</b>
												<div class="profile-desc-item pull-right">{{$user->email_kantor}}</div>
											</li>
											<li class="list-group-item">
												<b>Nilai KUM</b>
												<div class="profile-desc-item pull-right">{{$user->kum}}</div>
											</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Riwayat Kepegawaian</h3>
                                        <div class="card-options">
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                            <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                    <ul class="list-group">
											<li class="list-group-item">
												<b>NIP</b>
												<div class="profile-desc-item pull-right">{{$user->nip_users}}</div>
											</li>
											<li class="list-group-item">
												<b>Kartu Pegawai</b>
												<div class="profile-desc-item pull-right">{{$user->karpeg}}</div>
											</li>
											<li class="list-group-item">
												<b>Status Terbaru</b>
												<div class="profile-desc-item pull-right">{{isset($user->nidn)?'Dosen tetap':'kontrak/honorer'}}</div>
											</li>
											<li class="list-group-item">
												<b>Pangkat Terbaru</b>
												<div class="profile-desc-item pull-right">{{$pangkat_gol->pangkat??''}}</div>
											</li>
											<li class="list-group-item">
												<b>Golongan Terbaru</b>
												<div class="profile-desc-item pull-right">{{$pangkat_gol->golongan??''}}</div>
											</li>
											<li class="list-group-item">
												<b>Unit Kerja Terbaru</b>
												<div class="profile-desc-item pull-right">(belum tau diisi apa)</div>
											</li>
											<li class="list-group-item">
												<b>Jabatan Terbaru</b>
												<div class="profile-desc-item pull-right">{{$jabatan->jabatan??'Belum ada jabatan'}}</div>
											</li>
											<li class="list-group-item">
												<b>Perpanjangan</b>
												<div class="profile-desc-item pull-right">Belum ada</div>
											</li>
											<li class="list-group-item">
												<b>Pensiun</b>
												<div class="profile-desc-item pull-right">{{$user->pensiun}}</div>
											</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-8 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Riwayat Pendidikan</h3>
                                        <div class="card-options">
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                            <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                    <ul class="list-group">
											<li class="list-group-item">
												<b>Pendidikan Terakhir</b>
												<div class="profile-desc-item pull-right"><?= $user->riwayat_pendidikan->s3 ?? ($user->riwayat_pendidikan->s2 ?? $user->riwayat_pendidikan->s1);?></div>
											</li>
											<li class="list-group-item">
												<b>Strata S3</b>
												<div class="profile-desc-item pull-right">{{$user->riwayat_pendidikan->s3}}</div>
											</li>
											<li class="list-group-item">
												<b>Strata S2 / Spesialis</b>
												<div class="profile-desc-item pull-right">{{$user->riwayat_pendidikan->s2}}</div>
											</li>
											<li class="list-group-item">
												<b>Praktek Profesi</b>
												<div class="profile-desc-item pull-right">{{$user->riwayat_pendidikan->praktek_profesi}}</div>
											</li>
											<li class="list-group-item">
												<b>Strata S1</b>
												<div class="profile-desc-item pull-right">{{$user->riwayat_pendidikan->s1}}</div>
											</li>
											<li class="list-group-item">
												<b>Alumni SMA</b>
												<div class="profile-desc-item pull-right">{{$user->riwayat_pendidikan->sma}}</div>
											</li>
											<li class="list-group-item">
												<b>Alumni SMP</b>
												<div class="profile-desc-item pull-right">{{$user->riwayat_pendidikan->smp}}</div>
											</li>
											<li class="list-group-item">
												<b>Alumni SD</b>
												<div class="profile-desc-item pull-right">{{$user->riwayat_pendidikan->sd}}</div>
											</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Riwayat Tri Dharma</h3>
                                        <div class="card-options">
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                            <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <h6><b>A. PENDIDIKAN DAN PENGAJARAN</b></h6>
                                        <?php $count0 = 0;
                                        foreach ($user->tridharma_sks->where('biodata_id', $user->id) as $sks) {
                                            $temp[$count0] = $sks;
                                            $count0++;}?>
                                        <ul class="list-group">
											<li class="list-group-item">
												<b>SKS Mengajar Semester Ini</b>
												<div class="profile-desc-item pull-right"> {{$temp[0]->sks ?? 'Data tidak ditemukan'}}</div>
											</li>
											<li class="list-group-item">
												<b>SKS Total</b>
												<div class="profile-desc-item pull-right">
                                                    <?php $total_sks = 0;
                                                    for ($i=0; $i < $count0; $i++)
                                                        $total_sks += $temp[$i]->sks;
                                                        echo $total_sks;?>
                                                        </div>
											</li>
                                        </ul>
                                        <br>
                                        <h6><b>B. PENELITIAN DAN PENGEMBANGAN</b></h6>
                                        <?php $count1 = $count0;
                                        foreach ($user->tridharma_terbitan->where('biodata_id', $user->id) as $terbitan) {
                                            $temp[$count1] = $terbitan;
                                            $count1++;}?>
                                        <ul class="list-group">
											<li class="list-group-item">
												<b>Jurnal Terbaru</b>
												<div class="profile-desc-item pull-right">
                                                <?php for ($i=$count0; $i < $count1; $i++) {
                                                    if ($temp[$i]->jenis_terbitan == 'jurnal'){
                                                    echo $temp[$i]->judul;break;}}?></div>
											</li>
											<li class="list-group-item">
												<b>Buku Terbaru</b>
												<div class="profile-desc-item pull-right">
                                                <?php for ($i=$count0; $i < $count1; $i++) {
                                                    if ($temp[$i]->jenis_terbitan == 'buku'){
                                                    echo $temp[$i]->judul;break;}}?></div>
											</li>
                                        </ul>
                                        <br>
                                        <h6><b>C. PENGABDIAN MASYARAKAT</b></h6>
                                        <?php $count2 = $count1;
                                        foreach ($user->tridharma_pengabdian->where('biodata_id', $user->id) as $pengabdian) {
                                            $temp[$count2] = $pengabdian;
                                            $count2++;}?>
                                        <ul class="list-group">
											<li class="list-group-item">
												<b>Seminar Terbaru</b>
												<div class="profile-desc-item pull-right">
                                                <?php for ($i=$count1; $i < $count2; $i++) {
                                                    if ($temp[$i]->jenis_pengabdian == 'seminar'){
                                                    echo $temp[$i]->nama_kegiatan;break;}}?></div>
											</li>
											<li class="list-group-item">
												<b>Lokakarya Terbaru</b>
												<div class="profile-desc-item pull-right">
                                                    <?php for ($i=$count1; $i < $count2; $i++) {
                                                        if ($temp[$i]->jenis_pengabdian == 'lokakarya'){
                                                        echo $temp[$i]->nama_kegiatan;break;}}?></div>
											</li>
											<li class="list-group-item">
												<b>Proceeding Terbaru</b>
												<div class="profile-desc-item pull-right">
                                                <?php for ($i=$count0; $i < $count1; $i++) {
                                                    if ($temp[$i]->jenis_terbitan == 'proceeding'){
                                                    echo $temp[$i]->judul;break;}}?></div>
											</li>
											<li class="list-group-item">
												<b>Paten Terbaru</b>
												<div class="profile-desc-item pull-right">
                                                <?php for ($i=$count1; $i < $count2; $i++) {
                                                    if ($temp[$i]->jenis_pengabdian == 'paten'){
                                                    echo $temp[$i]->nama_kegiatan;break;}}?></div>
											</li>
											<li class="list-group-item">
												<b>Sertifikat HKI Terbaru</b>
												<div class="profile-desc-item pull-right">
                                                <?php for ($i=$count1; $i < $count2; $i++) {
                                                    if ($temp[$i]->jenis_pengabdian == 'hki'){
                                                    echo $temp[$i]->nama_kegiatan;break;}}?></div>
											</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @if (isset($user) && Auth::user()->role == 'admin')
                    <div class="tab-pane" id="pro-detail">
                        <div class="row clearfix row-deck">
                            <div class="col-lg-6 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Pangkat dan Jabatan</h3>
                                        <div class="card-options">
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                            <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                        </div>
                                    </div>
                                    <div class="table-responsive table-striped" style="height: 60%">
                                        <hr>
                                        <table class="table card-table table-vcenter text-nowrap table-striped mb-0">
                                            <tbody>
                                                <tr>
                                                    <th>Pangkat</th>
                                                    <th>Golongan</th>
                                                    <th>TMT</th>
                                                    <th>Nomor</th>
                                                    <th>Tanggal Sah</th>
                                                    <th>Pejabat</th>
                                                    <th>Gaji</th>
                                                </tr>
                                                @foreach ($user->riwayat_kepegawaian->where('jabatan',null) as $riwayat)
                                                <tr>
                                                    <td>{{$riwayat->pangkat}}</td>
                                                    <td>{{$riwayat->golongan}}</td>
                                                    <td>{{$riwayat->tmt}}</td>
                                                    <td>{{$riwayat->nomor}}</td>
                                                    <td>{{$riwayat->tgl_sah}}</td>
                                                    <td>{{$riwayat->pejabat}}</td>
                                                    <td>{{$riwayat->gaji}}</td>
                                                </tr>
                                                    @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <hr>
                                    <div class="table-responsive" style="height: 50%">
                                        <table class="table card-table table-vcenter text-nowrap table-striped mb-0">
                                            <tbody>
                                                <tr>
                                                    <th>Jabatan</th>
                                                    <th>TMT</th>
                                                    <th>Nomor</th>
                                                    <th>Tanggal Sah</th>
                                                    <th>Pejabat</th>
                                                    <th>Tunjangan</th>
                                                </tr>
                                                @foreach ($user->riwayat_kepegawaian->where('pangkat',null) as $riwayat)
                                                <tr>
                                                    <td>{{$riwayat->jabatan}}</td>
                                                    <td>{{$riwayat->tmt}}</td>
                                                    <td>{{$riwayat->nomor}}</td>
                                                    <td>{{$riwayat->tgl_sah}}</td>
                                                    <td>{{$riwayat->pejabat}}</td>
                                                    <td>{{$riwayat->tunjangan}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Keluarga, cpns, pns</h3>
                                        <div class="card-options">
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                            <a href="#" class="card-options-fullscreen" data-toggle="card-fullscreen"><i class="fe fe-maximize"></i></a>
                                        </div>
                                    </div>
                                    <div class="table-responsive" style="height: 40%;width: 100%">
                                        <hr>
                                        <h6 class="pl-3 mt-3">KELUARGA</h6>
                                        <table class="table card-table table-vcenter text-nowrap mb-0">
                                            <tbody>
                                                <tr>
                                                    <th>Nama 
                                                        @if (in_array(strtolower($user->kelamin), ['perempuan','wanita']))
                                                        Suami
                                                        @else
                                                        Istri
                                                        @endif
                                                    </th>
                                                    <td>{{$keluarga[0]->nama_pasangan}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <table class="table card-table table-vcenter text-nowrap table-hover mt-2">
                                            <thead>
                                                <tr>
                                                    <th>Anak</th>
                                                    <th>Tanggal Lahir</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($keluarga as $family)
                                                <tr>
                                                    <td>{{$family->nama_anak}}</td>
                                                    <td>{{date('d-m-Y',strtotime($family->ttl_anak))}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                        <hr>
                                    </div>
                                    <hr>
                                    <div class="table-responsive" style="height: 30%">
                                        <h6 class="pl-3 mt-3">CPNS</h6>
                                        <table class="table card-table table-vcenter text-nowrap table-hover">
                                            <thead>
                                                <tr>
                                                    <th>TMT</th>
                                                    <th>Nomor</th>
                                                    <th>Tanggal Sah</th>
                                                    <th>Pejabat</th>
                                                    <th>Gaji</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    @foreach ($cpns as $pns)
                                                    @if ($pns->jenis == 'cpns')
                                                    <td>{{$pns->tmt}}</td>
                                                    <td>{{$pns->nomor}}</td>
                                                    <td>{{date('d-m-Y',strtotime($pns->tgl_sah))}}</td>
                                                    <td>{{$pns->pejabat}}</td>
                                                    <td>Rp{{$pns->gaji}}</td>
                                                    @endif
                                                    @endforeach
                                                </tr>
                                            </tbody>
                                        </table>
                                        <hr>
                                    </div>
                                    <hr>
                                    <div class="table-responsive" style="height: 30%">
                                        <h6 class="pl-3 mt-3">PNS</h6>
                                        <table class="table card-table table-vcenter text-nowrap table-hover">
                                            <thead>
                                                <tr>
                                                    <th>TMT</th>
                                                    <th>Nomor</th>
                                                    <th>Tanggal Sah</th>
                                                    <th>Pejabat</th>
                                                    <th>Gaji</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    @foreach ($cpns as $pns)
                                                    @if ($pns->jenis == 'pns')
                                                    <td>{{$pns->tmt}}</td>
                                                    <td>{{$pns->nomor}}</td>
                                                    <td>{{date('d-m-Y',strtotime($pns->tgl_sah))}}</td>
                                                    <td>{{$pns->pejabat}}</td>
                                                    <td>Rp{{$pns->gaji}}</td>
                                                    @endif
                                                    @endforeach
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="pro-edit">
                        <div class="row clearfix">
                            <div class="col-md-6 col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Biodata</h3>
                                        <div class="card-options ">
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        </div>
                                    </div>
                                    <form action="/profil/bio" method="post" enctype="multipart/form-data">
                                        @csrf
                                        <div class="card-body">
                                            <div class="row clearfix">
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Nama Lengkap</label>
                                                        <input type="text" name="nama" class="form-control" value="{{$user->nama}}" onkeyup="this.value = this.value.toUpperCase();" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-3 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Tempat Lahir</label>
                                                        <input type="text" name="tmpt_lahir" class="form-control" value="{{$user->tmpt_lahir}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-4 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Tanggal Lahir</label>
                                                        <input data-provide="datepicker" name="tgl_lahir" data-date-autoclose="true" class="form-control" value="{{$user->tgl_lahir}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-5 col-sm-12">
                                                    <div class="form-group">
                                                        <label>No. KTP</label>
                                                        <input type="text" name="ktp" class="form-control" value="{{$user->nik}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Sertifikat Dosen</label>
                                                        <input type="text" name="npwp" class="form-control" value="{{$user->serdos}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <label>NIDN</label>
                                                        <input type="text" name="npwp" class="form-control" value="{{$user->nidn}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <label>No. NPWP</label>
                                                        <input type="text" name="npwp" class="form-control" value="{{$user->npwp_nomor}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <label>No. NPWP</label>
                                                        <input type="text" name="npwp" class="form-control" value="{{$user->alamat}}" disabled>
                                                    </div>
                                                </div>
                                                <div class="col-md-5 col-sm-12">
                                                    <label>Status Pernikahan</label>
                                                    <select class="form-control show-tick" name="pernikahan">
                                                        <option value="belum">Belum Menikah</option>
                                                        <option value="sudah" <?php if(strtolower($user->status_pernikahan) == "menikah") echo "selected"?>>Sudah Menikah</option>
                                                    </select>
                                                </div>
                                                <div class="col-md-7 col-sm-12">
                                                    <label>No. Buku Nikah</label>
                                                    <div class="form-group mt-2 mb-3">
                                                        <input type="file" id="buku_nikah" name="buku_nikah" class="dropify" accept=".jpg, .jpeg, .png, .pdf">
                                                        <small id="fileHelp" class="form-text text-muted">This is some placeholder block-level help text for the above input. It's a bit lighter and easily wraps to a new line.</small>
                                                    </div>
                                                </div>
                                                <div class="col-md-12 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Nama Suami/Istri</label>
                                                        <input type="text" name="nama_pasangan" class="form-control" value="{{$user->nama_pasangan}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Jenis Asuransi</label>
                                                        <input type="text" name="asuransi" class="form-control" value="{{$user->asuransi}}">
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <label>Alamat Email <span class="text-danger">*</span></label>
                                                        <div class="input-group">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="icon-envelope"></i></span>
                                                            </div>
                                                            <input type="text" name="email" class="form-control" value="{{$user->email}}" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Alamat Domisili <span class="text-danger">*</span></label>
                                                        <input type="text" name="domisili" class="form-control" value="{{$user->domisili}}" required>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Alamat Praktek</label>
                                                        <input type="text" name="praktek" class="form-control" value="{{$user->praktek_profesi}}">
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <button type="submit" class="btn btn-primary">Submit</button>
                                                    <a href="profile"><button type="button" class="btn btn-outline-secondary">Cancel</button></a>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Riwayat Kepegawaian</h3>
                                        <div class="card-options ">
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>NIP</label>
                                                    <input type="text" class="form-control" value="{{$user->nip_users}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Kartu Pegawai</label>
                                                    <input type="text" class="form-control" value="{{$user->karpeg}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Status</label>
                                                    <input type="text" class="form-control" value="{{isset($user->nidn)?'Dosen tetap':'kontrak/honorer'}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Unit Kerja Terbaru</label>
                                                    <input type="text" class="form-control" value="belom tahu" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Pangkat Terbaru</label>
                                                    <input type="text" class="form-control" value="{{isset($pangkat_gol->pangkat)?$pangkat_gol->pangkat:''}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Pangkat</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Golongan Terbaru</label>
                                                    <input type="text" class="form-control" value="{{isset($pangkat_gol->golongan)?$pangkat_gol->golongan:''}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Golongan</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Jabatan Terbaru</label>
                                                    <input type="text" class="form-control" value="{{isset($jabatan->jabatan)?$jabatan->jabatan:''}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Jabatan</label>
                                                    <input type="text" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label>TMT</label>
                                                    <input type="date" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-sm-12">
                                                <div class="form-group">
                                                    <label>Nomor</label>
                                                    <input type="text" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-12">
                                                <div class="form-group">
                                                    <label>Tanggal Sah</label>
                                                    <input type="date" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-md-8 col-sm-12">
                                                <div class="form-group">
                                                    <label>Pejabat</label>
                                                    <input type="text" class="form-control" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Perpanjangan</label>
                                                    <input type="text" class="form-control" value="Belum ada" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Pensiun</label>
                                                    <input data-provide="datepicker" data-date-autoclose="true" class="form-control" value="{{$user->pensiun}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <button type="submit" class="btn btn-outline-secondary">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title">Riwayat Pendidikan</h3>
                                        <div class="card-options ">
                                            <a href="#" class="card-options-collapse" data-toggle="card-collapse"><i class="fe fe-chevron-up"></i></a>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row clearfix">
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Alumni SD</label>
                                                    <input type="text" class="form-control" value="{{$user->riwayat_pendidikan->sd}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Alumni SMP</label>
                                                    <input type="text" class="form-control" value="{{$user->riwayat_pendidikan->smp}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Alumni SMA</label>
                                                    <input type="text" class="form-control" value="{{$user->riwayat_pendidikan->sma}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Strata S1</label>
                                                    <input type="text" class="form-control" value="{{$user->riwayat_pendidikan->s1}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Praktek Profesi</label>
                                                    <input type="text" class="form-control" value="{{$user->riwayat_pendidikan->praktek_profesi}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Strata S2</label>
                                                    <input type="text" class="form-control" value="{{$user->riwayat_pendidikan->s2}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label>Strata S3</label>
                                                    <input type="text" class="form-control" value="{{$user->riwayat_pendidikan->s3}}" disabled>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                <button type="submit" class="btn btn-outline-secondary">Cancel</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>                        
                    @endif
                </div>
            </div>
        </div>
        @endsection
