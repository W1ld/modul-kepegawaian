<?php

use App\Http\Controllers\loginController;
use App\Http\Controllers\mainController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/login', [loginController::class, 'index'])->name('login');
Route::post('/login', [loginController::class, 'authenticate']);
Route::post('/logout', [loginController::class, 'logout'])->name('logout');
Route::get('/profil', [mainController::class, 'profil'])->name('profil');

// Route::get('/markread', [mainController::class, 'markRead']);
// Route::get('/markread/{id}', [mainController::class, 'markRead'])->name('hapus-notif');

Route::middleware(['admin'])->group(function () {
    Route::get('/register', [loginController::class, 'register']);
    Route::post('/register', [loginController::class, 'store']);
    Route::get('/verifikasi', [mainController::class, 'request'])->name('request');
    Route::get('/verifikasi/{id}', [mainController::class, 'detailRequest']);
    Route::post('/verifikasi/w/{id}', [mainController::class, 'wRequest']);
    Route::get('/verifikasi/{assets}/{tridharma}/{filename}', [mainController::class, 'viewFile'])->name('media');
});

Route::middleware(['multirole:admin_dekan'])->group(function () {
    Route::get('/', [mainController::class, 'dashboard'])->name('dashboard');
    Route::get('/z', [mainController::class, 'dashboardz'])->name('dashboard');
    Route::get('/dosen', [mainController::class, 'dosen'])->name('dosen');
    Route::get('/dosen/{nip}', [mainController::class, 'detailDosen']);
});

Route::middleware(['multirole:dosen_dekan'])->group(function () {
    Route::post('/profil/bio', [mainController::class, 'editBiodata']);
    Route::get('/tridharma', [mainController::class, 'tridharma'])->name('tridharma');
    Route::post('/tridharma/sks', [mainController::class, 'tridharmaSKS']);
    Route::post('/tridharma/terbitan', [mainController::class, 'tridharmaTerbitan']);
    Route::post('/tridharma/pengabdian', [mainController::class, 'tridharmaPengabdian']);
});