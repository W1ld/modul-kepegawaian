<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TridharmaTerbitan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tridharma_terbitans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('biodata_id');
            $table->string('judul',100);
            $table->string('penulis',100);
            $table->string('penerbit',50);
            $table->string('jenis_penerbitan',13);
            $table->date('waktu_terbit');
            $table->smallInteger('halaman');
            $table->string('jenis_terbitan',10);
            $table->string('link_terbit',555)->nullable();
            $table->float('poin');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tridharma_terbitan');
    }
}