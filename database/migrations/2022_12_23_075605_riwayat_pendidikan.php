<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RiwayatPendidikan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_pendidikans', function (Blueprint $table) {
            $table->id();
            $table->foreignId('biodata_id');
            // $table->integer('id_dosen');
            // $table->foreign('id_dosen')->references('id')->on('biodata')->onDelete('cascade');
            $table->string('sd',50)->nullable();
            $table->string('smp',50)->nullable();
            $table->string('sma',50)->nullable();
            $table->string('s1',123);
            $table->string('praktek_profesi',123)->nullable();
            $table->string('s2',123)->nullable();
            $table->string('s3',123)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_pendidikan');
    }
}