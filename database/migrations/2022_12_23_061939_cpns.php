<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Cpns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cpns', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('nip_users');
            $table->date('tmt');
            $table->string('nomor',30);
            $table->date('tgl_sah');
            $table->mediumInteger('gaji');
            $table->string('pejabat',150);
            $table->string('jenis',4);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cpns');
    }
}