<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Keluarga extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keluargas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('nip_users');
            $table->string('nama_pasangan',50)->nullable();
            $table->string('ttl_pasangan',30)->nullable();
            $table->string('nik_pasangan',20)->nullable();
            $table->date('tgl_nikah')->nullable();
            $table->string('nama_anak',50)->nullable();
            $table->string('ttl_anak',30)->nullable();
            $table->string('nik_anak',20)->nullable();
            $table->string('status_anak',50)->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keluarga');
    }
}