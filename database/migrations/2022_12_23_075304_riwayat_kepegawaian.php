<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RiwayatKepegawaian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('riwayat_kepegawaians', function (Blueprint $table) {
            $table->id();
            $table->foreignId('biodata_id');
            $table->string('pangkat',30)->nullable();
            $table->string('golongan',5)->nullable();
            $table->string('jabatan',30)->nullable();
            $table->date('tmt');
            $table->string('nomor',30);
            $table->date('tgl_sah');
            $table->string('pejabat',100);
            $table->integer('gaji')->nullable();
            $table->integer('tunjangan')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('riwayat_kepegawaian');
    }
}