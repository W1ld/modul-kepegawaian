<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Biodata extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biodatas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('nip_users')->unique();
            // $table->foreign('nip_users')->references('nip')->on('users')->onDelete('cascade');
            $table->bigInteger('nip_lama_users')->nullable();
            $table->string('prodi',30);
            $table->string('nidn',20)->nullable();
            $table->string('serdos',20);
            $table->string('karpeg',8);
            $table->string('no_sumpeg', 20)->nullable();
            $table->date('tgl_sumpeg')->nullable();
            $table->string('pejabat_sumpeg',100)->nullable();
            $table->string('nik',16);
            $table->string('foto',100);
            $table->string('nama',50);
            $table->date('tgl_lahir');
            $table->string('tmpt_lahir',20);
            $table->string('kelamin',9);
            $table->string('agama',20);
            $table->string('no_hp',15);
            $table->string('email_pribadi',30);
            $table->string('email_kantor',30);
            $table->string('alamat_rumah',100);
            $table->string('alamat_kantor',100);
            $table->string('alamat_praktek',100)->nullable();
            $table->string('foto_ktp',100)->nullable();
            $table->string('npwp_nomor',25)->nullable();
            $table->string('npwp_alamat',100)->nullable();
            $table->string('bpjs_nomor',20)->nullable();
            $table->string('bpjs_fasyankes',20)->nullable();
            $table->string('status_pernikahan',10)->nullable();
            $table->string('buku_nikah',100)->nullable();
            $table->string('pensiun',20)->nullable();
            $table->float('kum');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biodata');
    }
}