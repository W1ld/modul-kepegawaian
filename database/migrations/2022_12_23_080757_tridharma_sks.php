<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TridharmaSks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tridharma_sks', function (Blueprint $table) {
            $table->id();
            $table->foreignId('biodata_id');
            $table->tinyInteger('semester');
            $table->string('tahun_ajaran',9);
            $table->tinyInteger('sks');
            $table->float('poin');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tridharma_sks');
    }
}