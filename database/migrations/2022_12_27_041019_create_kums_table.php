<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKumsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kums', function (Blueprint $table) {
            $table->id();
            $table->string('komponen_kegiatan');
            $table->string('kategori')->nullable();
            $table->string('batas_maksimal_diakui')->nullable();
            $table->float('angka_kredit')->nullable();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kums');
    }
}