<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ReqHistory extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('req_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('biodata_id');
            $table->string('judul',100)->nullable();
            $table->string('penulis',100)->nullable();
            $table->string('penerbit',50)->nullable();
            $table->string('jenis_penerbitan',13)->nullable();
            $table->date('waktu_terbit')->nullable();
            $table->smallInteger('halaman')->nullable();
            $table->string('jenis_terbitan',10)->nullable();
            $table->string('link_terbit',555)->nullable();
            $table->tinyInteger('semester')->nullable();
            $table->string('tahun_ajaran',9)->nullable();
            $table->tinyInteger('sks')->nullable();
            $table->string('nama_kegiatan',100)->nullable();
            $table->year('tahun')->nullable();
            $table->bigInteger('nomor_sertif')->nullable();
            $table->string('foto',62)->nullable();
            $table->string('jenis_pengabdian',9)->nullable();
            $table->float('poin')->nullable();
            $table->string('alasan')->nullable();
            $table->tinyInteger('type')->nullable();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('req_history');
    }
}