<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TridharmaPengabdian extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tridharma_pengabdians', function (Blueprint $table) {
            $table->id();
            $table->foreignId('biodata_id');
            $table->string('nama_kegiatan',100);
            $table->year('tahun');
            $table->bigInteger('nomor_sertif')->nullable();
            $table->string('foto',62)->nullable();
            $table->string('jenis_pengabdian',9);
            $table->float('poin');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrentOnUpdate()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tridharma_pengabdian');
    }
}