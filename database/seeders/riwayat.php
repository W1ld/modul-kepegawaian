<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class riwayat extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('riwayat_kepegawaians')->insert([
            'id'=>1,
            'biodata_id'=>1,
            'pangkat'=>'',
            'golongan'=>NULL,
            'jabatan'=>'',
            'tmt'=>'2022-12-23',
            'nomor'=>'',
            'tgl_sah'=>'2022-12-23',
            'pejabat'=>'',
            'gaji'=>0,
            'tunjangan'=>0
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>2,
            'biodata_id'=>2,
            'pangkat'=>'',
            'golongan'=>NULL,
            'jabatan'=>'',
            'tmt'=>'2022-12-23',
            'nomor'=>'',
            'tgl_sah'=>'2022-12-23',
            'pejabat'=>'',
            'gaji'=>0,
            'tunjangan'=>0
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>3,
            'biodata_id'=>3,
            'pangkat'=>'',
            'golongan'=>NULL,
            'jabatan'=>'',
            'tmt'=>'2022-12-23',
            'nomor'=>'',
            'tgl_sah'=>'2022-12-23',
            'pejabat'=>'',
            'gaji'=>0,
            'tunjangan'=>0
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>8,
            'biodata_id'=>4,
            'pangkat'=>'Penata Muda TK I',
            'golongan'=>'III/b',
            'jabatan'=>NULL,
            'tmt'=>'2022-12-23',
            'nomor'=>'425/J03.11/KP/2003',
            'tgl_sah'=>'2003-01-09',
            'pejabat'=>'Mendiknas',
            'gaji'=>828200,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>9,
            'biodata_id'=>4,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Asisten Ahli',
            'tmt'=>'2001-01-01',
            'nomor'=>'3130/J03/KP/2001',
            'tgl_sah'=>'2001-03-20',
            'pejabat'=>'Mendiknas',
            'gaji'=>NULL,
            'tunjangan'=>270000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>12,
            'biodata_id'=>5,
            'pangkat'=>'Penata Muda TK I',
            'golongan'=>'III/b',
            'jabatan'=>NULL,
            'tmt'=>'1994-10-01',
            'nomor'=>'1046/PT03.H2/C/1995',
            'tgl_sah'=>'1994-10-01',
            'pejabat'=>'Mendikbud',
            'gaji'=>213200,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>13,
            'biodata_id'=>5,
            'pangkat'=>'Penata',
            'golongan'=>'III/c',
            'jabatan'=>NULL,
            'tmt'=>'2001-04-01',
            'nomor'=>'10947/JO03/KP/2001',
            'tgl_sah'=>'2001-04-01',
            'pejabat'=>'Mendiknas',
            'gaji'=>970900,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>14,
            'biodata_id'=>5,
            'pangkat'=>'Penata TK I',
            'golongan'=>'III/d',
            'jabatan'=>NULL,
            'tmt'=>'2012-10-01',
            'nomor'=>'16312/H3/KP/2012',
            'tgl_sah'=>'2012-10-01',
            'pejabat'=>'Mendikbud',
            'gaji'=>3426200,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>15,
            'biodata_id'=>5,
            'pangkat'=>'Pembina',
            'golongan'=>'IV/a',
            'jabatan'=>NULL,
            'tmt'=>'2014-10-01',
            'nomor'=>'171144/A4.3/KP/2014',
            'tgl_sah'=>'2014-10-01',
            'pejabat'=>'Mendikbud',
            'gaji'=>4222300,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>16,
            'biodata_id'=>5,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Asisten Ahli',
            'tmt'=>'1994-08-01',
            'nomor'=>'7156/PT03.H/C/1994',
            'tgl_sah'=>'1994-07-04',
            'pejabat'=>'Mendikbud',
            'gaji'=>NULL,
            'tunjangan'=>180000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>17,
            'biodata_id'=>5,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor',
            'tmt'=>'2001-01-01',
            'nomor'=>'2998/JO3/KP/2001',
            'tgl_sah'=>'2001-03-20',
            'pejabat'=>'Mendiknas',
            'gaji'=>NULL,
            'tunjangan'=>353900
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>18,
            'biodata_id'=>5,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor Kepala',
            'tmt'=>'2011-02-01',
            'nomor'=>'8206/A4.3/KP/2011',
            'tgl_sah'=>'2011-01-31',
            'pejabat'=>'Mendiknas',
            'gaji'=>NULL,
            'tunjangan'=>900000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>19,
            'biodata_id'=>6,
            'pangkat'=>'Penata Muda TK I',
            'golongan'=>'III/b',
            'jabatan'=>NULL,
            'tmt'=>'2002-01-01',
            'nomor'=>'1956/J03.11/KP/2002',
            'tgl_sah'=>'2002-01-01',
            'pejabat'=>'Mendiknas',
            'gaji'=>848900,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>20,
            'biodata_id'=>6,
            'pangkat'=>'Penata',
            'golongan'=>'III/c',
            'jabatan'=>NULL,
            'tmt'=>'2005-04-01',
            'nomor'=>'2657/JO3/KP/2005',
            'tgl_sah'=>'2005-04-01',
            'pejabat'=>'Mendiknas',
            'gaji'=>1075100,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>21,
            'biodata_id'=>6,
            'pangkat'=>'Penata TK I',
            'golongan'=>'III/d',
            'jabatan'=>NULL,
            'tmt'=>'2009-10-01',
            'nomor'=>'1438/H3/KP/2009',
            'tgl_sah'=>'2009-10-01',
            'pejabat'=>'Mendiknas',
            'gaji'=>2168700,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>22,
            'biodata_id'=>6,
            'pangkat'=>'Pembina',
            'golongan'=>'IV/a',
            'jabatan'=>NULL,
            'tmt'=>'2021-04-01',
            'nomor'=>'58384/A3/KP.06.00/2021',
            'tgl_sah'=>'2021-04-01',
            'pejabat'=>'Mendikbud RISTEK',
            'gaji'=>4416700,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>23,
            'biodata_id'=>6,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Asisten Ahli',
            'tmt'=>'2001-01-01',
            'nomor'=>'3131/J03/KP/2001',
            'tgl_sah'=>'2001-03-20',
            'pejabat'=>'Mendiknas',
            'gaji'=>NULL,
            'tunjangan'=>125000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>24,
            'biodata_id'=>6,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor',
            'tmt'=>'2005-01-01',
            'nomor'=>'33/J03/KP/2004',
            'tgl_sah'=>'2004-12-29',
            'pejabat'=>'Mendiknas',
            'gaji'=>NULL,
            'tunjangan'=>502500
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>25,
            'biodata_id'=>6,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor Kepala',
            'tmt'=>'2020-04-01',
            'nomor'=>'42209/A3/KP/2020',
            'tgl_sah'=>'2020-04-23',
            'pejabat'=>'Mendikbud',
            'gaji'=>NULL,
            'tunjangan'=>900000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>26,
            'biodata_id'=>8,
            'pangkat'=>'Penata Muda TK I',
            'golongan'=>'III/b',
            'jabatan'=>NULL,
            'tmt'=>'1981-10-01',
            'nomor'=>'3777/PT03.1/C/1985/1',
            'tgl_sah'=>'1982-03-15',
            'pejabat'=>'Mendikbud',
            'gaji'=>39500,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>27,
            'biodata_id'=>8,
            'pangkat'=>'Penata',
            'golongan'=>'III/c',
            'jabatan'=>NULL,
            'tmt'=>'1984-10-01',
            'nomor'=>'419/PT03.1/C/1985/1',
            'tgl_sah'=>'1985-01-22',
            'pejabat'=>'Mendikbud',
            'gaji'=>49200,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>28,
            'biodata_id'=>8,
            'pangkat'=>'Penata TK I',
            'golongan'=>'III/d',
            'jabatan'=>NULL,
            'tmt'=>'1997-10-01',
            'nomor'=>'8338/J03/KP/2001',
            'tgl_sah'=>'1997-10-24',
            'pejabat'=>'Mendikbud',
            'gaji'=>442000,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>29,
            'biodata_id'=>8,
            'pangkat'=>'Pembina',
            'golongan'=>'IV/a',
            'jabatan'=>NULL,
            'tmt'=>'2001-04-01',
            'nomor'=>'66618/A2.III.1/KP/2001',
            'tgl_sah'=>'2001-07-30',
            'pejabat'=>'Mendiknas',
            'gaji'=>483900,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>30,
            'biodata_id'=>8,
            'pangkat'=>'Pembina TK I',
            'golongan'=>'IV/b',
            'jabatan'=>NULL,
            'tmt'=>'2010-10-01',
            'nomor'=>'102760/A4.5/KP/2010',
            'tgl_sah'=>'2010-12-20',
            'pejabat'=>'Mendiknas',
            'gaji'=>3161600,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>31,
            'biodata_id'=>8,
            'pangkat'=>'Pembina Utama Muda',
            'golongan'=>'IV/c',
            'jabatan'=>NULL,
            'tmt'=>'2012-10-01',
            'nomor'=>'89/K TAHUN 2012',
            'tgl_sah'=>'2012-11-14',
            'pejabat'=>'PRESIDEN RI',
            'gaji'=>4237600,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>32,
            'biodata_id'=>8,
            'pangkat'=>'Pembina Utama Madya',
            'golongan'=>'IV/d',
            'jabatan'=>NULL,
            'tmt'=>'2015-04-01',
            'nomor'=>'00210/KEP/AA/12016/15',
            'tgl_sah'=>'2015-05-20',
            'pejabat'=>'PRESIDEN RI',
            'gaji'=>5086900,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>33,
            'biodata_id'=>8,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor',
            'tmt'=>'1997-05-01',
            'nomor'=>'50302/A2.IV.1/KP/1997',
            'tgl_sah'=>'1997-04-30',
            'pejabat'=>'KOORDINATOR URUSAN PENDIDIKAN MENENGAH DAN PENDIDIKAN TINGGI',
            'gaji'=>NULL,
            'tunjangan'=>157000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>34,
            'biodata_id'=>8,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor Kepala',
            'tmt'=>'2001-01-01',
            'nomor'=>'21957/A2.III.1/KP/2001',
            'tgl_sah'=>'2001-03-20',
            'pejabat'=>'Mendiknas',
            'gaji'=>NULL,
            'tunjangan'=>645000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>35,
            'biodata_id'=>8,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Guru Besar',
            'tmt'=>'2010-06-01',
            'nomor'=>'55773/A4.5/KP/2010',
            'tgl_sah'=>'2010-05-31',
            'pejabat'=>'Mendiknas',
            'gaji'=>NULL,
            'tunjangan'=>1350000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>36,
            'biodata_id'=>9,
            'pangkat'=>'Penata',
            'golongan'=>'III/c',
            'jabatan'=>NULL,
            'tmt'=>'2011-04-01',
            'nomor'=>'1565/H3/KP/2011',
            'tgl_sah'=>'2011-05-30',
            'pejabat'=>'Mendiknas',
            'gaji'=>2501500,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>37,
            'biodata_id'=>9,
            'pangkat'=>'Penata TK I',
            'golongan'=>'III/d',
            'jabatan'=>NULL,
            'tmt'=>'2019-04-01',
            'nomor'=>'19704/M/KP/2019',
            'tgl_sah'=>'2019-05-28',
            'pejabat'=>'MENRISTEKDIKTI',
            'gaji'=>3912600,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>38,
            'biodata_id'=>9,
            'pangkat'=>'Pembina',
            'golongan'=>'IV/a',
            'jabatan'=>NULL,
            'tmt'=>'2021-04-01',
            'nomor'=>'27193/A3/KP.06.00/2021',
            'tgl_sah'=>'2021-06-11',
            'pejabat'=>'Mendikbud',
            'gaji'=>4461700,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>39,
            'biodata_id'=>9,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor',
            'tmt'=>'2010-12-01',
            'nomor'=>'1515/H3/KP/2010',
            'tgl_sah'=>'2010-11-30',
            'pejabat'=>'Mendiknas',
            'gaji'=>NULL,
            'tunjangan'=>700000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>40,
            'biodata_id'=>9,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor Kepala',
            'tmt'=>'2018-08-01',
            'nomor'=>'34237/A2.3/KP/2018',
            'tgl_sah'=>'2018-06-25',
            'pejabat'=>'MENRISTEKDIKTI',
            'gaji'=>NULL,
            'tunjangan'=>900000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>41,
            'biodata_id'=>7,
            'pangkat'=>'Penata Muda TK I',
            'golongan'=>'III/b',
            'jabatan'=>NULL,
            'tmt'=>'2002-01-01',
            'nomor'=>'1956/J03.11/KP/2002',
            'tgl_sah'=>'2002-01-01',
            'pejabat'=>'Mendiknas',
            'gaji'=>848900,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>42,
            'biodata_id'=>7,
            'pangkat'=>'Penata',
            'golongan'=>'III/c',
            'jabatan'=>NULL,
            'tmt'=>'2005-04-01',
            'nomor'=>'2657/JO3/KP/2005',
            'tgl_sah'=>'2005-04-01',
            'pejabat'=>'Mendiknas',
            'gaji'=>1075100,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>43,
            'biodata_id'=>7,
            'pangkat'=>'Penata TK I',
            'golongan'=>'III/d',
            'jabatan'=>NULL,
            'tmt'=>'2009-10-01',
            'nomor'=>'1438/H3/KP/2009',
            'tgl_sah'=>'2009-10-01',
            'pejabat'=>'Mendiknas',
            'gaji'=>2168700,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>44,
            'biodata_id'=>7,
            'pangkat'=>'Pembina',
            'golongan'=>'IV/a',
            'jabatan'=>NULL,
            'tmt'=>'2021-04-01',
            'nomor'=>'58384/A3/KP.06.00/2021',
            'tgl_sah'=>'2021-04-01',
            'pejabat'=>'MendikbudRISTEK',
            'gaji'=>4416700,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>45,
            'biodata_id'=>7,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Asisten Ahli',
            'tmt'=>'2001-01-01',
            'nomor'=>'3131/J03/KP/2001',
            'tgl_sah'=>'2001-03-20',
            'pejabat'=>'Mendiknas',
            'gaji'=>NULL,
            'tunjangan'=>125000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>46,
            'biodata_id'=>7,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor',
            'tmt'=>'2005-01-01',
            'nomor'=>'33/J03/KP/2004',
            'tgl_sah'=>'2004-12-29',
            'pejabat'=>'Mendiknas',
            'gaji'=>NULL,
            'tunjangan'=>502500
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>47,
            'biodata_id'=>7,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor Kepala',
            'tmt'=>'2020-04-01',
            'nomor'=>'42209/A3/KP/2020',
            'tgl_sah'=>'2020-04-23',
            'pejabat'=>'Mendikbud',
            'gaji'=>NULL,
            'tunjangan'=>900000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>48,
            'biodata_id'=>1,
            'pangkat'=>'Penata Muda TK I',
            'golongan'=>'III/b',
            'jabatan'=>NULL,
            'tmt'=>'1996-04-01',
            'nomor'=>'6605/J03/KP/1996',
            'tgl_sah'=>'1996-08-16',
            'pejabat'=>'Mendikbud',
            'gaji'=>213200,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>49,
            'biodata_id'=>1,
            'pangkat'=>'Penata',
            'golongan'=>'III/c',
            'jabatan'=>NULL,
            'tmt'=>'1998-04-01',
            'nomor'=>'5197/J03/KP/1998',
            'tgl_sah'=>'1998-07-15',
            'pejabat'=>'Mendikbud',
            'gaji'=>333800,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>50,
            'biodata_id'=>1,
            'pangkat'=>'Penata TK I',
            'golongan'=>'III/d',
            'jabatan'=>NULL,
            'tmt'=>'2004-04-01',
            'nomor'=>'3711/J03/KP/2004',
            'tgl_sah'=>'2004-05-14',
            'pejabat'=>'Mendiknas',
            'gaji'=>1224800,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>51,
            'biodata_id'=>1,
            'pangkat'=>'Pembina',
            'golongan'=>'IV/a',
            'jabatan'=>NULL,
            'tmt'=>'2009-10-01',
            'nomor'=>'93078/A4.5/KP/2009',
            'tgl_sah'=>'2009-12-30',
            'pejabat'=>'Mendiknas',
            'gaji'=>2490700,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>52,
            'biodata_id'=>1,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Asisten Ahli',
            'tmt'=>'1995-11-01',
            'nomor'=>'8846/PT03.H/C/1995',
            'tgl_sah'=>'1995-10-20',
            'pejabat'=>'Mendikbud',
            'gaji'=>NULL,
            'tunjangan'=>180000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>53,
            'biodata_id'=>1,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor',
            'tmt'=>'2001-01-01',
            'nomor'=>'3084/J03/KP/2001',
            'tgl_sah'=>'2001-03-20',
            'pejabat'=>'Mendiknas',
            'gaji'=>NULL,
            'tunjangan'=>502000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>54,
            'biodata_id'=>1,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor Kepala dmk Radiologi',
            'tmt'=>'2009-09-01',
            'nomor'=>'71531/A4.5/KP/2009',
            'tgl_sah'=>'2009-08-31',
            'pejabat'=>'Mendiknas',
            'gaji'=>NULL,
            'tunjangan'=>900000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>55,
            'biodata_id'=>2,
            'pangkat'=>'Penata Muda TK I',
            'golongan'=>'III/b',
            'jabatan'=>NULL,
            'tmt'=>'2001-10-01',
            'nomor'=>'11302/J03.11/KP/2001',
            'tgl_sah'=>'2001-12-20',
            'pejabat'=>'MENDIKBUD',
            'gaji'=>937000,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>56,
            'biodata_id'=>2,
            'pangkat'=>'Penata',
            'golongan'=>'III/c',
            'jabatan'=>NULL,
            'tmt'=>'2012-01-04',
            'nomor'=>'5703/H3/KP/2012',
            'tgl_sah'=>'2012-05-07',
            'pejabat'=>'MENDIKBUD',
            'gaji'=>3191900,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>57,
            'biodata_id'=>2,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Asisten Ahli',
            'tmt'=>'2001-06-01',
            'nomor'=>'1382/JO3/KP/2001',
            'tgl_sah'=>'2001-05-21',
            'pejabat'=>'MENDIKBUD',
            'gaji'=>NULL,
            'tunjangan'=>270000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>58,
            'biodata_id'=>2,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor',
            'tmt'=>'2012-02-01',
            'nomor'=>'1200/H3/KP/2012',
            'tgl_sah'=>'2012-01-31',
            'pejabat'=>'MENDIKNAS',
            'gaji'=>NULL,
            'tunjangan'=>700000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>59,
            'biodata_id'=>10,
            'pangkat'=>'Penata Muda Tk. I',
            'golongan'=>'III/b',
            'jabatan'=>NULL,
            'tmt'=>'1994-10-01',
            'nomor'=>'844/PT03.H2/C/1995',
            'tgl_sah'=>'1994-10-01',
            'pejabat'=>'MENDIKBUD',
            'gaji'=>198400,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>60,
            'biodata_id'=>10,
            'pangkat'=>'Penata',
            'golongan'=>'III/c',
            'jabatan'=>NULL,
            'tmt'=>'1998-04-01',
            'nomor'=>'6558 J03 KP 1998',
            'tgl_sah'=>'1998-04-01',
            'pejabat'=>'MENDIKBUD',
            'gaji'=>333800,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>61,
            'biodata_id'=>10,
            'pangkat'=>'Penata Tk. I',
            'golongan'=>'III/d',
            'jabatan'=>NULL,
            'tmt'=>'2001-04-01',
            'nomor'=>'21490/A2.7/KP/2004',
            'tgl_sah'=>'2001-04-01',
            'pejabat'=>'MENDIKNAS',
            'gaji'=>1005900,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>62,
            'biodata_id'=>10,
            'pangkat'=>'Pembina',
            'golongan'=>'IV/a',
            'jabatan'=>NULL,
            'tmt'=>'2004-04-01',
            'nomor'=>'21490/A2 7/ KP/ 2004',
            'tgl_sah'=>'2004-04-01',
            'pejabat'=>'MENDIKNAS',
            'gaji'=>1276600,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>63,
            'biodata_id'=>10,
            'pangkat'=>'Pembina Tk. I',
            'golongan'=>'IV/b',
            'jabatan'=>NULL,
            'tmt'=>'2016-10-01',
            'nomor'=>'70081/A2.3/KP/2016',
            'tgl_sah'=>'2016-10-01',
            'pejabat'=>'MENRISTEKDIKTI',
            'gaji'=>4665000,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>64,
            'biodata_id'=>10,
            'pangkat'=>'Pembina Utama Muda',
            'golongan'=>'IV/c',
            'jabatan'=>NULL,
            'tmt'=>'2018-10-01',
            'nomor'=>'00107/KEP/AA/15001/18',
            'tgl_sah'=>'2018-10-01',
            'pejabat'=>'PRESIDEN RI',
            'gaji'=>5015400,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>65,
            'biodata_id'=>10,
            'pangkat'=>'Pembina Utama Madya',
            'golongan'=>'IV/d',
            'jabatan'=>NULL,
            'tmt'=>'2020-10-01',
            'nomor'=>'00108/KEP/AA/15001/20',
            'tgl_sah'=>'2020-10-01',
            'pejabat'=>'PRESIDEN RI',
            'gaji'=>5015400,
            'tunjangan'=>NULL
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>66,
            'biodata_id'=>10,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Asisten Ahli',
            'tmt'=>'1993-12-01',
            'nomor'=>'5503/PT03H/C/1993',
            'tgl_sah'=>'1993-09-25',
            'pejabat'=>'A.N. MENTERI PENDIDIKAN DAN KEBUDAYAAN REKTOR UNIVERSITAS AIRLANGGA',
            'gaji'=>NULL,
            'tunjangan'=>180000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>67,
            'biodata_id'=>10,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor',
            'tmt'=>'2001-02-01',
            'nomor'=>'262/J00/KP/2001',
            'tgl_sah'=>'2001-03-30',
            'pejabat'=>'REKTOR UNAIR',
            'gaji'=>NULL,
            'tunjangan'=>502000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>68,
            'biodata_id'=>10,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Lektor Kepala',
            'tmt'=>'2003-12-01',
            'nomor'=>'38012/A2.7/KP/2003',
            'tgl_sah'=>'2003-01-12',
            'pejabat'=>'A.N MENTERI PENDIDIKAN NASIONAL KEPALA BIRO KEPEGAWAIAN SEKRETARIAT JENDRAL',
            'gaji'=>NULL,
            'tunjangan'=>645000
            ] );
            
            
                        
            DB::table('riwayat_kepegawaians')->insert([
            'id'=>69,
            'biodata_id'=>10,
            'pangkat'=>NULL,
            'golongan'=>NULL,
            'jabatan'=>'Guru Besar',
            'tmt'=>'2019-08-01',
            'nomor'=>'28726/M/KP/2019',
            'tgl_sah'=>'2019-02-09',
            'pejabat'=>'MOHAMAD NASIR',
            'gaji'=>NULL,
            'tunjangan'=>1350000
            ] );

        DB::table('riwayat_pendidikans')->insert([
            'id'=>3,
            'biodata_id'=>3,
            'sd'=>'sd kiri',
            'smp'=>'smp kiri',
            'sma'=>'sma kiri',
            's1'=>'S1 MANAGEMENT UNIVERSITAS ISLAM NEGERI SUNAN AMPEL SURABAYA',
            'praktek_profesi'=>'TOKO BAROKAH SELALU, JALAN ABOGA 19 NOMOR 4',
            's2'=>'S2 MANAGEMEN INDUSTRI UNIVERSITY OF SYDNEY',
            's3'=>'S3 MANAGEMENT HARVARD UNIVERSITY'
        ]);
        DB::table('riwayat_pendidikans')->insert([
            'id'=>4,
            'biodata_id'=>4,
            'sd'=>NULL,
            'smp'=>NULL,
            'sma'=>NULL,
            's1'=>'SKG FKG Tahun 1994',
            'praktek_profesi'=>'Pendidikan Dokter Gigi Universitas Airlangga Tahun 1994',
            's2'=>'Spesialis Bedah Mulut & Maksilofasial Universitas Airlangga Tahun 2006',
            's3'=>NULL
        ]);
        DB::table('riwayat_pendidikans')->insert([
            'id'=>5,
            'biodata_id'=>5,
            'sd'=>NULL,
            'smp'=>NULL,
            'sma'=>NULL,
            's1'=>'SKG FKG Tahun 2000',
            'praktek_profesi'=>'Pendidikan Dokter Gigi Universitas Airlangga Tahun 2001',
            's2'=>'Spesialis Bedah Mulut dan Maksilofasial FKG Universitas Airlangga Tahun 2010',
            's3'=>'Doktor FK Unair Tahun 2016'
        ]);
        DB::table('riwayat_pendidikans')->insert([
            'id'=>6,
            'biodata_id'=>6,
            'sd'=>NULL,
            'smp'=>NULL,
            'sma'=>NULL,
            's1'=>'KEDOKTERAN GIGI UNAIR, 1984',
            'praktek_profesi'=>'KEDOKTERAN GIGI UNAIR, 1984',
            's2'=>'ILMU KESEHATAN GIGI UNAIR, 1999',
            's3'=>NULL
        ]);
        DB::table('riwayat_pendidikans')->insert([
            'id'=>7,
            'biodata_id'=>7,
            'sd'=>NULL,
            'smp'=>NULL,
            'sma'=>NULL,
            's1'=>'KEDOKTERAN GIGI UNAIR, 1994',
            'praktek_profesi'=>'KEDOKTERAN GIGI UNAIR, 1994',
            's2'=>'ILMU KESEHAAN GIGI UNAIR, 2004',
            's3'=>NULL
        ]);
        DB::table('riwayat_pendidikans')->insert([
            'id'=>8,
            'biodata_id'=>8,
            'sd'=>NULL,
            'smp'=>NULL,
            'sma'=>NULL,
            's1'=>'PENDIDIKAN DOKTER GIGI, FKG UNAIR',
            'praktek_profesi'=>'KEDOKTERAN GIGI, FKG UNAIR',
            's2'=>'ILMU KEDOKTERAN GIGI,UGM',
            's3'=>'DENTAL SCIENCE, HIROSHIMA UNIVERSITY'
        ]);
        DB::table('riwayat_pendidikans')->insert([
            'id'=>9,
            'biodata_id'=>9,
            'sd'=>'SD Gubeng Klisingan',
            'smp'=>'SMP Negeri 12 Surabaya',
            'sma'=>'SMA Negeri 5 Surabaya',
            's1'=>'Kedokteran Gigi UNAIR',
            'praktek_profesi'=>'Kedokteran Gigi UNAIR',
            's2'=>'S2 Kesehatan Masyarakat UNAIR',
            's3'=>'S3 Doctor Of Philosophy The UNIVERSITY OF ADELAIDE'
        ]);
        DB::table('riwayat_pendidikans')->insert([
            'id'=>10,
            'biodata_id'=>1,
            'sd'=>NULL,
            'smp'=>NULL,
            'sma'=>NULL,
            's1'=>'Dokter Gigi Universitas Airlangga Tahun 1985',
            'praktek_profesi'=>'Dokter Gigi Spesialis 1 bidang Radiologi Kedokteran Gigi oleh Majelis Kolegium Kedokteran Gigi Indonesia Tahun 2004',
            's2'=>'Magister Kesehatan Universitas Airlangga Tahun 1995',
            's3'=>'Doktor Universitas Airlangga Tahun 2005'
        ]);
        DB::table('riwayat_pendidikans')->insert([
            'id'=>11,
            'biodata_id'=>2,
            'sd'=>NULL,
            'smp'=>NULL,
            'sma'=>NULL,
            's1'=>'KEDOKTERAN GIGI LULUS 1986',
            'praktek_profesi'=>'KEDOKTERAN GIGI UNAIR',
            's2'=>'MANCHESTER DENTAL SCOOL LULUS 1996',
            's3'=>'SPESIALIS ORTODONSIA LULUS 2005'
        ]);
        DB::table('riwayat_pendidikans')->insert([
            'id'=>12,
            'biodata_id'=>10,
            'sd'=>'1975',
            'smp'=>'1976',
            'sma'=>'1979',
            's1'=>'1984',
            'praktek_profesi'=>'1985',
            's2'=>'1997',
            's3'=>'2008'
        ]);
        DB::table('riwayat_pendidikans')->insert([
            'id'=>13,
            'biodata_id'=>11,
            'sd'=>NULL,
            'smp'=>NULL,
            'sma'=>NULL,
            's1'=>'1989',
            'praktek_profesi'=>'1992',
            's2'=>'2003',
            's3'=>'2021'
        ]);
    }
}