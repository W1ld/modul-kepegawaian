<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class keluarga extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('keluargas')->insert([
            'id'=>1,
            'nip_users'=>'196904051996011001',
            'nama_pasangan'=>'drg Dyani Budhita',
            'ttl_pasangan'=>'Surabaya, 13/05/1968',
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>'1995-05-26',
            'nama_anak'=>'R. Agastya Ardhitaputera R',
            'ttl_anak'=>'01 Februari 1997',
            'nik_anak'=>NULL,
            'status_anak'=>'Anak kandung'
        ]);
        DB::table('keluargas')->insert([
            'id'=>2,
            'nip_users'=>'196904051996011001',
            'nama_pasangan'=>'drg Dyani Budhita',
            'ttl_pasangan'=>'Surabaya, 13/05/1968',
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>'1995-05-26',
            'nama_anak'=>'R. Devathara Ardhisatrya',
            'ttl_anak'=>'03 Desember 1998',
            'nik_anak'=>NULL,
            'status_anak'=>'Anak kandung'
        ]);
        DB::table('keluargas')->insert([
            'id'=>3,
            'nip_users'=>'197803292005012001',
            'nama_pasangan'=>'Ixnatius Ariyando',
            'ttl_pasangan'=>'19 January 1979',
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>'2007-08-10',
            'nama_anak'=>'Kirana Eka Shanti Ariyando',
            'ttl_anak'=>'16 Juni 2008',
            'nik_anak'=>NULL,
            'status_anak'=>'Anak Kandung'
        ]);
        DB::table('keluargas')->insert([
            'id'=>4,
            'nip_users'=>'196004091986012001',
            'nama_pasangan'=>'UDAHERWIBOWO',
            'ttl_pasangan'=>'SURABAYA, 31-08-1959',
            'nik_pasangan'=>'3578223108590000',
            'tgl_nikah'=>'1985-04-13',
            'nama_anak'=>'RAHMADHANI DIAN PRATIWI',
            'ttl_anak'=>'SURABAYA, 23-02-93',
            'nik_anak'=>'3578222302930000',
            'status_anak'=>'SEKOLAH'
        ]);
        DB::table('keluargas')->insert([
            'id'=>5,
            'nip_users'=>'196703061996011001',
            'nama_pasangan'=>'SITI KHODIJAH',
            'ttl_pasangan'=>'17 December 1981',
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>'1997-07-19',
            'nama_anak'=>'THORIQ MAJIDO ANOOR',
            'ttl_anak'=>'SURABAYA, 23-08-94',
            'nik_anak'=>NULL,
            'status_anak'=>'SEKOLAH'
        ]);
        DB::table('keluargas')->insert([
            'id'=>6,
            'nip_users'=>'196703061996011001',
            'nama_pasangan'=>NULL,
            'ttl_pasangan'=>NULL,
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>NULL,
            'nama_anak'=>'MUHAMMAD ILLHAM RAMDLANI',
            'ttl_anak'=>'SURABAYA, 17-12-89',
            'nik_anak'=>NULL,
            'status_anak'=>'SEKOLAH'
        ]);
        DB::table('keluargas')->insert([
            'id'=>7,
            'nip_users'=>'196703061996011001',
            'nama_pasangan'=>NULL,
            'ttl_pasangan'=>NULL,
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>NULL,
            'nama_anak'=>'NABILAH RATNADUHILA',
            'ttl_anak'=>'SURABAYA, 27-03-2000',
            'nik_anak'=>NULL,
            'status_anak'=>'SEKOLAH'
        ]);
        DB::table('keluargas')->insert([
            'id'=>8,
            'nip_users'=>'196703061996011001',
            'nama_pasangan'=>NULL,
            'ttl_pasangan'=>NULL,
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>NULL,
            'nama_anak'=>'MUHAMMAD SYAMSUDDIN HAFIZH',
            'ttl_anak'=>'SURABAYA, 12-11-2002',
            'nik_anak'=>NULL,
            'status_anak'=>'SEKOLAH'
        ]);
        DB::table('keluargas')->insert([
            'id'=>9,
            'nip_users'=>'196703061996011001',
            'nama_pasangan'=>NULL,
            'ttl_pasangan'=>NULL,
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>NULL,
            'nama_anak'=>'RIJAL AZMI AMRULLAH',
            'ttl_anak'=>'SURABAYA, 12-06-2004',
            'nik_anak'=>NULL,
            'status_anak'=>'SEKOLAH'
        ]);
        DB::table('keluargas')->insert([
            'id'=>10,
            'nip_users'=>'196703061996011001',
            'nama_pasangan'=>NULL,
            'ttl_pasangan'=>NULL,
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>NULL,
            'nama_anak'=>'MUHAMMAD ADYAN FAQIHUDDIN',
            'ttl_anak'=>'SURABAYA, 18-11-2006',
            'nik_anak'=>NULL,
            'status_anak'=>'SEKOLAH'
        ]);
        DB::table('keluargas')->insert([
            'id'=>11,
            'nip_users'=>'196703061996011001',
            'nama_pasangan'=>NULL,
            'ttl_pasangan'=>NULL,
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>NULL,
            'nama_anak'=>'MUHAMMAD NAUFAL RAFIUDDIN',
            'ttl_anak'=>'SURABAYA, 10-02-2009',
            'nik_anak'=>NULL,
            'status_anak'=>'SEKOLAH'
        ]);
        DB::table('keluargas')->insert([
            'id'=>12,
            'nip_users'=>'195207161978031002',
            'nama_pasangan'=>'Hj. YOELI ELMERILLIA, SE',
            'ttl_pasangan'=>'21/07/1967',
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>NULL,
            'nama_anak'=>'M.LUTFI WICAKSONO',
            'ttl_anak'=>NULL,
            'nik_anak'=>NULL,
            'status_anak'=>'Anak Kandung'
        ]);
        DB::table('keluargas')->insert([
            'id'=>13,
            'nip_users'=>'195207161978031002',
            'nama_pasangan'=>NULL,
            'ttl_pasangan'=>NULL,
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>NULL,
            'nama_anak'=>'ALYA KAHMA THRISNA',
            'ttl_anak'=>NULL,
            'nik_anak'=>NULL,
            'status_anak'=>'Anak Kandung'
        ]);
        DB::table('keluargas')->insert([
            'id'=>14,
            'nip_users'=>'197112112008121003',
            'nama_pasangan'=>'Rizka Maulida, SH',
            'ttl_pasangan'=>' 07 Mei 1972',
            'nik_pasangan'=>'3578064705720002',
            'tgl_nikah'=>'1999-04-11',
            'nama_anak'=>'Athaya Syahira Ramadhani',
            'ttl_anak'=>'Surabaya, 2 Januari 2000',
            'nik_anak'=>'3578064201000004',
            'status_anak'=>'Belum Menikah'
        ]);
        DB::table('keluargas')->insert([
            'id'=>15,
            'nip_users'=>'197112112008121003',
            'nama_pasangan'=>NULL,
            'ttl_pasangan'=>NULL,
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>NULL,
            'nama_anak'=>'Fadiya Asyifa Zahrani',
            'ttl_anak'=>'Surabaya, 26 April 2003',
            'nik_anak'=>'3578066604030010',
            'status_anak'=>'Belum Menikah'
        ]);
        DB::table('keluargas')->insert([
            'id'=>16,
            'nip_users'=>'11111111',
            'nama_pasangan'=>'Ir Darmadjaja',
            'ttl_pasangan'=>'1961-12-28',
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>'1987-12-27',
            'nama_anak'=>'Nurudin Firmansyah Putra',
            'ttl_anak'=>'1992-04-12',
            'nik_anak'=>NULL,
            'status_anak'=>'Anak Kandung'
        ]);
        DB::table('keluargas')->insert([
            'id'=>17,
            'nip_users'=>'22222222',
            'nama_pasangan'=>NULL,
            'ttl_pasangan'=>NULL,
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>NULL,
            'nama_anak'=>'Rurin Fitria Yunita',
            'ttl_anak'=>'Surabaya, 09-06-1986',
            'nik_anak'=>NULL,
            'status_anak'=>NULL
        ]);
        DB::table('keluargas')->insert([
            'id'=>18,
            'nip_users'=>'22222222',
            'nama_pasangan'=>NULL,
            'ttl_pasangan'=>NULL,
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>NULL,
            'nama_anak'=>'Rully Karina Yulita',
            'ttl_anak'=>'Surabaya, 01-07-1987',
            'nik_anak'=>NULL,
            'status_anak'=>NULL
        ]);
        DB::table('keluargas')->insert([
            'id'=>19,
            'nip_users'=>'22222222',
            'nama_pasangan'=>NULL,
            'ttl_pasangan'=>NULL,
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>NULL,
            'nama_anak'=>'RUDI YULIO ARINDIONO',
            'ttl_anak'=>'Surabaya, 19-07-1991',
            'nik_anak'=>NULL,
            'status_anak'=>NULL
        ]);
        DB::table('keluargas')->insert([
            'id'=>20,
            'nip_users'=>'196108091987012001',
            'nama_pasangan'=>'FARICH AMIN',
            'ttl_pasangan'=>'23/08/1957',
            'nik_pasangan'=>'3578242308570001',
            'tgl_nikah'=>'1970-01-01',
            'nama_anak'=>' AFIFALIZIA',
            'ttl_anak'=>'MEDAN, 16/07/1995',
            'nik_anak'=>'3578245607960006',
            'status_anak'=>'MAHASISWA'
        ]);
        DB::table('keluargas')->insert([
            'id'=>21,
            'nip_users'=>'196309071990022001',
            'nama_pasangan'=>' IR. R. SOERIANANDA SATJADIBRATA',
            'ttl_pasangan'=>'1959-11-06',
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>'1989-12-29',
            'nama_anak'=>' RIANDA AZARINA',
            'ttl_anak'=>'SURABAYA,10/11/1990',
            'nik_anak'=>NULL,
            'status_anak'=>'blm menikah'
        ]);
        DB::table('keluargas')->insert([
            'id'=>22,
            'nip_users'=>'196309071990022001',
            'nama_pasangan'=>NULL,
            'ttl_pasangan'=>NULL,
            'nik_pasangan'=>NULL,
            'tgl_nikah'=>NULL,
            'nama_anak'=>' MUHAMMAD DEVANDA',
            'ttl_anak'=>'SURABAYA, 03/01/1992',
            'nik_anak'=>NULL,
            'status_anak'=>'blm menikah'
        ]);
    }
}