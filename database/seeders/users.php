<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id'=>4,
            'name'=>'admin',
            'nip'=>'00000000',
            'email'=>'admin@adm.in',
            'email_verified_at'=>NULL,
            'password'=>'$2y$10$zJuebfjNJ4hPkZjpKCWxgebKXFSKGeolIqico6sYe/RYyndp1a532',
            'remember_token'=>NULL,
            'foto'=>NULL,
            'created_at'=>'2022-12-05 19:08:13',
            'updated_at'=>'2022-12-05 19:08:13',
            'role'=>'admin'
        ]);
        DB::table('users')->insert([
            'id'=>1,
            'name'=>'dekan',
            'nip'=>'11111111',
            'email'=>'dekan@mail.com',
            'email_verified_at'=>NULL,
            'password'=>'$2y$10$utNL.JpSw9xJDDNqLMw55e7ZhPo3FAE11H6LAcn1QZRERcuOgrScm',
            'remember_token'=>NULL,
            'foto'=>'../assets/images/users/user3-128x128.jpg',
            'created_at'=>'2022-12-05 04:17:47',
            'updated_at'=>'2022-12-05 04:17:47',
            'role'=>'dekan'
        ]);
        DB::table('users')->insert([
            'id'=>2,
            'name'=>'dosen',
            'nip'=>'22222222',
            'email'=>'dosen@mail.id',
            'email_verified_at'=>NULL,
            'password'=>'$2y$10$tjPRbqrMzXdIYM/F7/3ZnuVd6r3p9aDZMU.FglN.Femwbr0oDDa0K',
            'remember_token'=>NULL,
            'foto'=>'../assets/images/users/user7-128x128.jpg',
            'created_at'=>'2022-12-05 04:19:32',
            'updated_at'=>'2022-12-05 04:19:32',
            'role'=>'dosen'
        ]);
        DB::table('users')->insert([
            'id'=>3,
            'name'=>'tendik',
            'nip'=>'33333333',
            'email'=>'tendik@mail.id',
            'email_verified_at'=>NULL,
            'password'=>'$2y$10$2XYZrYjIoMJhedk6uswDh.ZhlDwgy8/uCIQlhL1F2EAGtxN4b4rK2',
            'remember_token'=>NULL,
            'foto'=>'../assets/images/users/user8-128x128.jpg',
            'created_at'=>'2022-12-05 04:20:03',
            'updated_at'=>'2022-12-05 04:20:03',
            'role'=>'tendik'
        ]);
    }
}