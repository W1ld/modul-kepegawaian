<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class kum extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // A1
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'doktor',
            'kategori'=>'gelar pendidikan formal',
            'batas_maksimal_diakui'=>'1/periode penilaian',
            'angka_kredit'=>200
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'magister',
            'kategori'=>'gelar pendidikan formal',
            'batas_maksimal_diakui'=>'1/periode penilaian',
            'angka_kredit'=>150
        ]);
        // A2
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'diklat golongan 3',
            'kategori'=>'gelar pendidikan formal',
            'batas_maksimal_diakui'=>'1/periode penilaian',
            'angka_kredit'=>3
        ]);

        // B1
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'beban mengajar 10 sks pertama',
            'kategori'=>'asisten ahli',
            'batas_maksimal_diakui'=>'5',
            'angka_kredit'=>0.5
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'beban mengajar 2 sks berikutnya',
            'kategori'=>'asisten ahli',
            'batas_maksimal_diakui'=>'0.5',
            'angka_kredit'=>0.25
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'beban mengajar 10 sks pertama',
            'kategori'=>'Lektor/Lektor Kepala/Profesor',
            'batas_maksimal_diakui'=>'10/semester',
            'angka_kredit'=>'1'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'beban mengajar 2 sks berikutnya',
            'kategori'=>'Lektor/Lektor Kepala/Profesor',
            'batas_maksimal_diakui'=>'1/semester',
            'angka_kredit'=>'0.5'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'melakukan pengajaran untuk peserta pendidikan dokter melalui tindakan medik 
            spesialistik',
            'kategori'=>'Kegiatan pelaksanaan pendidikan untuk pendidikan dokter klinis',
            'batas_maksimal_diakui'=>'11/semester',
            'angka_kredit'=>'4'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'melakukan pengajaran konsultasi spesialis kepada peserta pendidikan dokter',
            'kategori'=>'Kegiatan pelaksanaan pendidikan untuk pendidikan dokter klinis',
            'batas_maksimal_diakui'=>'11/semester',
            'angka_kredit'=>'2'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>' Melakukan pemeriksaan luar dengan pembimbingan terhadap peserta pendidikan dokter',
            'kategori'=>'Kegiatan pelaksanaan pendidikan untuk pendidikan dokter klinis',
            'batas_maksimal_diakui'=>'11/semester',
            'angka_kredit'=>'2'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Melakukan pemeriksaan dalam dengan pembimbingan terhadap peserta pendidikan dokter',
            'kategori'=>'Kegiatan pelaksanaan pendidikan untuk pendidikan dokter klinis',
            'batas_maksimal_diakui'=>'11/semester',
            'angka_kredit'=>'3'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Menjadi saksi ahli dengan pembimbingan terhadap peserta pendidikan dokter',
            'kategori'=>'Kegiatan pelaksanaan pendidikan untuk pendidikan dokter klinis',
            'batas_maksimal_diakui'=>'11/semester',
            'angka_kredit'=>'1'
        ]);

        // B2
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Membimbing seminar mahasiswa',
            'kategori'=>null,
            'batas_maksimal_diakui'=>'setiap semester',
            'angka_kredit'=>'1'
        ]);

        // B3
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Membimbing KKN, Praktik Kerja Nyata, Praktik Kerja Lapangan',
            'kategori'=>null,
            'batas_maksimal_diakui'=>'setiap semester',
            'angka_kredit'=>'1'
        ]);

        // B4
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Pembimbing Utama Disertasi',
            'kategori'=>'Membimbing dan ikut membimbing dalam menghasilkan disertasi, tesis, skripsi dan laporan akhir studi yang sesuai bidang penugasannya',
            'batas_maksimal_diakui'=>'lulusan / semester',
            'angka_kredit'=>'8'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Pembimbing Utama Tesis',
            'kategori'=>'Membimbing dan ikut membimbing dalam menghasilkan disertasi, tesis, skripsi dan laporan akhir studi yang sesuai bidang penugasannya',
            'batas_maksimal_diakui'=>'lulusan / semester',
            'angka_kredit'=>'3'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Pembimbing Utama Skripsi',
            'kategori'=>'Membimbing dan ikut membimbing dalam menghasilkan disertasi, tesis, skripsi dan laporan akhir studi yang sesuai bidang penugasannya',
            'batas_maksimal_diakui'=>'lulusan / semester',
            'angka_kredit'=>'1'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Pembimbing Utama Laporan akhir studi',
            'kategori'=>'Membimbing dan ikut membimbing dalam menghasilkan disertasi, tesis, skripsi dan laporan akhir studi yang sesuai bidang penugasannya',
            'batas_maksimal_diakui'=>'lulusan / semester',
            'angka_kredit'=>'1'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Pembimbing Pendamping Disertasi',
            'kategori'=>'Membimbing dan ikut membimbing dalam menghasilkan disertasi, tesis, skripsi dan laporan akhir studi yang sesuai bidang penugasannya',
            'batas_maksimal_diakui'=>'lulusan / semester',
            'angka_kredit'=>'6'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Pembimbing Pendamping Tesis',
            'kategori'=>'Membimbing dan ikut membimbing dalam menghasilkan disertasi, tesis, skripsi dan laporan akhir studi yang sesuai bidang penugasannya',
            'batas_maksimal_diakui'=>'lulusan / semester',
            'angka_kredit'=>'2'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Pembimbing Pendamping Skripsi',
            'kategori'=>'Membimbing dan ikut membimbing dalam menghasilkan disertasi, tesis, skripsi dan laporan akhir studi yang sesuai bidang penugasannya',
            'batas_maksimal_diakui'=>'lulusan / semester',
            'angka_kredit'=>'0.5'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Pembimbing Pendamping Laporan akhir studi',
            'kategori'=>'Membimbing dan ikut membimbing dalam menghasilkan disertasi, tesis, skripsi dan laporan akhir studi yang sesuai bidang penugasannya',
            'batas_maksimal_diakui'=>'lulusan / semester',
            'angka_kredit'=>'0.5'
        ]);

        // B5
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Ketua penguji',
            'kategori'=>'Penguji pada ujian akhir/Profesi',
            'batas_maksimal_diakui'=>'4 lulusan / semester',
            'angka_kredit'=>'1'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Anggota penguji',
            'kategori'=>'Penguji pada ujian akhir/Profesi',
            'batas_maksimal_diakui'=>'8 lulusan / semester',
            'angka_kredit'=>'0.5'
        ]);

        // B6
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Membina kegiatan mahasiswa di bidang akademik dan kemahasiswaan, termasuk dalam kegiatan ini adalah membimbing mahasiswa menghasilkan produk saintifik',
            'kategori'=>null,
            'batas_maksimal_diakui'=>'2 kegiatan / semester',
            'angka_kredit'=>'2'
        ]);

        // B7
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Mengembangkan program kuliah yang mempunyai nilai kebaharuan metode atau substansi',
            'kategori'=>null,
            'batas_maksimal_diakui'=>'1 mata kuliah / semester',
            'angka_kredit'=>'2'
        ]);

        // B8
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'buku ajar',
            'kategori'=>'Mengembangkan bahan pengajaran/bahan kuliah yang mempunyai nilai kebaharuan',
            'batas_maksimal_diakui'=>'1 mata kuliah / semester',
            'angka_kredit'=>'20'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Diktat, Modul, Petunjuk praktikum, Model, Alat bantu, Audio visual, Naskah tutorial, Job sheet praktikum terkait dengan mata kuliah yang diampu',
            'kategori'=>'Mengembangkan bahan pengajaran/bahan kuliah yang mempunyai nilai kebaharuan',
            'batas_maksimal_diakui'=>'1 produk / semester',
            'angka_kredit'=>'5'
        ]);

        // B9
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Menyampaikan orasi ilmiah di tingkat perguruan tinggi',
            'kategori'=>null,
            'batas_maksimal_diakui'=>'2 orasi / semester',
            'angka_kredit'=>'5'
        ]);

        // B10
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Rektor',
            'kategori'=>'Menduduki jabatan pimpinan perguruan tinggi sesuai tugas pokok, fungsi dan kewenangan dan/atau setara',
            'batas_maksimal_diakui'=>'1 jabatan / semester',
            'angka_kredit'=>'6'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Wakil rektor/dekan/direktur program pasca sarjana/ketua lembaga',
            'kategori'=>'Menduduki jabatan pimpinan perguruan tinggi sesuai tugas pokok, fungsi dan kewenangan dan/atau setara',
            'batas_maksimal_diakui'=>'1 jabatan / semester',
            'angka_kredit'=>'5'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Ketua sekolah tinggi/pembantu dekan/asisten direktur program pasca sarjana/direktur politeknik/kepala LLDikti',
            'kategori'=>'Menduduki jabatan pimpinan perguruan tinggi sesuai tugas pokok, fungsi dan kewenangan dan/atau setara',
            'batas_maksimal_diakui'=>'1 jabatan / semester',
            'angka_kredit'=>'4'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Pembantu ketua sekolah tinggi/pembantu direktur politeknik',
            'kategori'=>'Menduduki jabatan pimpinan perguruan tinggi sesuai tugas pokok, fungsi dan kewenangan dan/atau setara',
            'batas_maksimal_diakui'=>'1 jabatan / semester',
            'angka_kredit'=>'4'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Direktur akademi',
            'kategori'=>'Menduduki jabatan pimpinan perguruan tinggi sesuai tugas pokok, fungsi dan kewenangan dan/atau setara',
            'batas_maksimal_diakui'=>'1 jabatan / semester',
            'angka_kredit'=>'4'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Pembantu direktur politeknik, ketua jurusan/bagian pada universitas/institut/sekolah tinggi',
            'kategori'=>'Menduduki jabatan pimpinan perguruan tinggi sesuai tugas pokok, fungsi dan kewenangan dan/atau setara',
            'batas_maksimal_diakui'=>'1 jabatan / semester',
            'angka_kredit'=>'3'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Pembantu direktur akademi/ketua jurusan/ketua prodi pada universitas/politeknik/akademi, sekretaris jurusan/bagian pada universitas/institut/sekolah tinggi',
            'kategori'=>'Menduduki jabatan pimpinan perguruan tinggi sesuai tugas pokok, fungsi dan kewenangan dan/atau setara',
            'batas_maksimal_diakui'=>'1 jabatan / semester',
            'angka_kredit'=>'3'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Sekretaris jurusan pada politeknik/akademi dan kepala laboratorium (bengkel) universitas/institut/sekolah tinggi/politeknik/akademi',
            'kategori'=>'Menduduki jabatan pimpinan perguruan tinggi sesuai tugas pokok, fungsi dan kewenangan dan/atau setara',
            'batas_maksimal_diakui'=>'1 jabatan / semester',
            'angka_kredit'=>'3'
        ]);

        // B11
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Pembimbing pencangkokan',
            'kategori'=>'Membimbing dosen yang mempunyai jabatan akademik lebih rendah setiap semester (bagi dosen Lektor Kepala ke atas)',
            'batas_maksimal_diakui'=>'1 orang',
            'angka_kredit'=>'2'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Reguler',
            'kategori'=>'Membimbing dosen yang mempunyai jabatan akademik lebih rendah setiap semester (bagi dosen Lektor Kepala ke atas)',
            'batas_maksimal_diakui'=>'1 orang',
            'angka_kredit'=>'1'
        ]);

        // B12
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Detasering',
            'kategori'=>'Melaksanakan kegiatan detasering dan pencangkokan di luar institusi tempat bekerja setiap semester (bagi dosen Lektor kepala ke atas)',
            'batas_maksimal_diakui'=>'1 orang',
            'angka_kredit'=>'5'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Pencangkokan',
            'kategori'=>'Melaksanakan kegiatan detasering dan pencangkokan di luar institusi tempat bekerja setiap semester (bagi dosen Lektor kepala ke atas)',
            'batas_maksimal_diakui'=>'1 orang',
            'angka_kredit'=>'4'
        ]);

        // B13
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Lamanya lebih dari 690 jam',
            'kategori'=>'Melaksanakan pengembangan diri untuk meningkatkan kompetensi',
            'batas_maksimal_diakui'=>null,
            'angka_kredit'=>'15'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Lamanya antara 641-960 jam',
            'kategori'=>'Melaksanakan pengembangan diri untuk meningkatkan kompetensi',
            'batas_maksimal_diakui'=>null,
            'angka_kredit'=>'9'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Lamanya antara 481-640 jam',
            'kategori'=>'Melaksanakan pengembangan diri untuk meningkatkan kompetensi',
            'batas_maksimal_diakui'=>null,
            'angka_kredit'=>'6'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Lamanya antara 161-480 jam',
            'kategori'=>'Melaksanakan pengembangan diri untuk meningkatkan kompetensi',
            'batas_maksimal_diakui'=>null,
            'angka_kredit'=>'3'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Lamanya antara 81-160 jam',
            'kategori'=>'Melaksanakan pengembangan diri untuk meningkatkan kompetensi',
            'batas_maksimal_diakui'=>null,
            'angka_kredit'=>'2'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Lamanya antara 30-80 jam',
            'kategori'=>'Melaksanakan pengembangan diri untuk meningkatkan kompetensi',
            'batas_maksimal_diakui'=>null,
            'angka_kredit'=>'1'
        ]);
        DB::table('kums')->insert([
            'komponen_kegiatan'=>'Lamanya antara 10-30 jam',
            'kategori'=>'Melaksanakan pengembangan diri untuk meningkatkan kompetensi',
            'batas_maksimal_diakui'=>null,
            'angka_kredit'=>'0.5'
        ]);
    }
}