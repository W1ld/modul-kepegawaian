<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class cpns extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cpns')->insert([
            'id'=>3,
            'nip_users'=>'196904051996011001',
            'tmt'=>'1996-01-01',
            'nomor'=>'84244/A2/KP/1995',
            'tgl_sah'=>'1995-12-29',
            'gaji'=>120160,
            'pejabat'=>'Mendikbud',
            'jenis'=>'cpns'            
        ]);
        DB::table('cpns')->insert([
            'id'=>4,
            'nip_users'=>'196904051996011001',
            'tmt'=>'1997-06-01',
            'nomor'=>'3642/J03/KP/1997',
            'tgl_sah'=>'1997-05-19',
            'gaji'=>150200,
            'pejabat'=>'Mendikbud',
            'jenis'=>'pns'
        ]);
        DB::table('cpns')->insert([
            'id'=>5,
            'nip_users'=>' 197803292005012001',
            'tmt'=>'2005-01-01',
            'nomor'=>'8639/A2/KP/2005',
            'tgl_sah'=>'2005-03-22',
            'gaji'=>724320,
            'pejabat'=>'Mendiknas',
            'jenis'=>'cpns'
        ]);
        DB::table('cpns')->insert([
            'id'=>6,
            'nip_users'=>' 197803292005012001',
            'tmt'=>'2006-02-01',
            'nomor'=>'475/J03.11/KP/2006',
            'tgl_sah'=>'2006-02-19',
            'gaji'=>905400,
            'pejabat'=>'Mendiknas',
            'jenis'=>'pns'
        ]);
        DB::table('cpns')->insert([
            'id'=>7,
            'nip_users'=>'196004091986012001',
            'tmt'=>'1986-01-01',
            'nomor'=>'2164/PT03.1/C/1986',
            'tgl_sah'=>'1986-03-15',
            'gaji'=>64800,
            'pejabat'=>'Mendikbud',
            'jenis'=>'cpns'
        ]);
        DB::table('cpns')->insert([
            'id'=>8,
            'nip_users'=>'196004091986012001',
            'tmt'=>'1988-01-01',
            'nomor'=>'10473/PT03.H2/C/1987',
            'tgl_sah'=>'1987-12-21',
            'gaji'=>88500,
            'pejabat'=>'Mendikbud',
            'jenis'=>'pns'
        ]);
        DB::table('cpns')->insert([
            'id'=>9,
            'nip_users'=>'196703061996011001',
            'tmt'=>'1996-01-01',
            'nomor'=>'84245/A2/KP/1995',
            'tgl_sah'=>'1995-12-29',
            'gaji'=>120160,
            'pejabat'=>'KABAG Kepangkatan Tenaga Edukatif Pada Koordinator urs. Pendidikan Menengah dan Pendidikan Tinggi Biro Kepegawaian',
            'jenis'=>'cpns'
        ]);
        DB::table('cpns')->insert([
            'id'=>10,
            'nip_users'=>'196703061996011001',
            'tmt'=>'1997-06-01',
            'nomor'=>'640/J03/KP/1997',
            'tgl_sah'=>'1997-05-19',
            'gaji'=>150200,
            'pejabat'=>'Pembantu Rektor II UNAIR',
            'jenis'=>'pns'
        ]);
        DB::table('cpns')->insert([
            'id'=>11,
            'nip_users'=>'195207161978031002',
            'tmt'=>'1978-03-01',
            'nomor'=>'36145/C/2/78',
            'tgl_sah'=>'1978-05-15',
            'gaji'=>1227280,
            'pejabat'=>'Mendikbud',
            'jenis'=>'cpns'
        ]);
        DB::table('cpns')->insert([
            'id'=>12,
            'nip_users'=>'195207161978031002',
            'tmt'=>'1979-04-01',
            'nomor'=>'UP/976/UA/561/1979',
            'tgl_sah'=>'1979-03-31',
            'gaji'=>2034100,
            'pejabat'=>'Mendikbud',
            'jenis'=>'pns'
        ]);
        DB::table('cpns')->insert([
            'id'=>13,
            'nip_users'=>'197112112008121003',
            'tmt'=>'2008-12-01',
            'nomor'=>'43287/A4/KP/2009',
            'tgl_sah'=>'2009-05-29',
            'gaji'=>1349200,
            'pejabat'=>'Mendiknas',
            'jenis'=>'cpns'
        ]);
        DB::table('cpns')->insert([
            'id'=>14,
            'nip_users'=>'197112112008121003',
            'tmt'=>'2010-07-01',
            'nomor'=>'1029/H3/KP/2010',
            'tgl_sah'=>'2010-06-23',
            'gaji'=>2101800,
            'pejabat'=>'Mendiknas',
            'jenis'=>'pns'
        ]);
        DB::table('cpns')->insert([
            'id'=>15,
            'nip_users'=>'11111111',
            'tmt'=>'1988-03-01',
            'nomor'=>'3905/PT03.H15/C/1988',
            'tgl_sah'=>'1988-06-01',
            'gaji'=>64800,
            'pejabat'=>'Mendikbud',
            'jenis'=>'cpns'
        ]);
        DB::table('cpns')->insert([
            'id'=>16,
            'nip_users'=>'11111111',
            'tmt'=>'1989-11-01',
            'nomor'=>'7913/PT03.H2/C/1989',
            'tgl_sah'=>'1989-10-18',
            'gaji'=>81000,
            'pejabat'=>'Mendikbud',
            'jenis'=>'pns'
        ]);
        DB::table('cpns')->insert([
            'id'=>17,
            'nip_users'=>'22222222',
            'tmt'=>'1987-03-01',
            'nomor'=>'5472/PT.03.H15/C/1987',
            'tgl_sah'=>'1987-07-18',
            'gaji'=>64800,
            'pejabat'=>'KABIRO Adm. Umum UNAIR',
            'jenis'=>'cpns'
        ]);
        DB::table('cpns')->insert([
            'id'=>18,
            'nip_users'=>'22222222',
            'tmt'=>'1988-10-01',
            'nomor'=>'7053/PT.03.H2/C/1988',
            'tgl_sah'=>'1988-09-10',
            'gaji'=>81,
            'pejabat'=>'Pembantu Rektor II',
            'jenis'=>'pns'
        ]);
        DB::table('cpns')->insert([
            'id'=>19,
            'nip_users'=>'196108091987012001',
            'tmt'=>'1987-01-01',
            'nomor'=>'2474/PT03.H15/C/1987',
            'tgl_sah'=>'1887-04-01',
            'gaji'=>64800,
            'pejabat'=>'mendikbud',
            'jenis'=>'cpns'
        ]);
        DB::table('cpns')->insert([
            'id'=>20,
            'nip_users'=>'196108091987012001',
            'tmt'=>'1988-10-01',
            'nomor'=>'7055/PT03.H2/C/1988',
            'tgl_sah'=>'1988-09-10',
            'gaji'=>81000,
            'pejabat'=>'mendikbud',
            'jenis'=>'pns'
        ]);
        DB::table('cpns')->insert([
            'id'=>21,
            'nip_users'=>'196309071990022001',
            'tmt'=>'1983-03-01',
            'nomor'=>'65015/C/2/1983',
            'tgl_sah'=>'1983-06-15',
            'gaji'=>27280,
            'pejabat'=>'MENDIKBUD',
            'jenis'=>'cpns'
        ]);
        DB::table('cpns')->insert([
            'id'=>22,
            'nip_users'=>'196309071990022001',
            'tmt'=>'1990-02-01',
            'nomor'=>' 2288/PT03.H15/C/1990',
            'tgl_sah'=>'1990-03-14',
            'gaji'=>64800,
            'pejabat'=>'MENDIKBUD',
            'jenis'=>'cpns'
        ]);
        DB::table('cpns')->insert([
            'id'=>23,
            'nip_users'=>'196309071990022001',
            'tmt'=>'1984-08-01',
            'nomor'=>'4012/PT03.1/C/1984',
            'tgl_sah'=>'1984-07-25',
            'gaji'=>44200,
            'pejabat'=>'MENDIKBUD',
            'jenis'=>'pns'
        ]);
        DB::table('cpns')->insert([
            'id'=>24,
            'nip_users'=>'196309071990022001',
            'tmt'=>'1991-07-01',
            'nomor'=>'4983/PT03.H2/C/1991',
            'tgl_sah'=>'1991-06-29',
            'gaji'=>81000,
            'pejabat'=>'mendikbud',
            'jenis'=>'pns'
        ]);
    }
}